<?php

class PakatAccount
{
    private static $pakatAccountObj = null;
    private $pakatAccountData;
    private $lastResponseCode;

    /**
     * PakatAccount private constructor.
     */
    private function __construct()
    {

    }

    /**
     * Getter function for account data
     */
    public function getPakatAccountData()
    {
        return $this->pakatAccountData;
    }

    /**
     * Setter function for account data
     */
    public function setPakatAccountData($pakatAccountData)
    {
        $this->pakatAccountData = $pakatAccountData;
    }

    /**
     * Getter function for last response code
     */
    public function getLastResponseCode()
    {
        return $this->lastResponseCode;
    }

    /**
     * Setter function for last response code
     */
    public function setLastResponseCode($lastResponseCode)
    {
        $this->lastResponseCode = $lastResponseCode;
    }

    /**
     * Static function to create a new instance or return an existing instance.
     */
    public static function getInstance()
    {
        if( null == self::$pakatAccountObj )
        {
            self::$pakatAccountObj = new PakatAccount();
        }

        return self::$pakatAccountObj;
    }
}
