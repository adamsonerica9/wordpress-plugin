<?php
/**
 * Manage Pakat API
 *
 * Use wp API transient to reduce loading time of API call
 *
 * @package Pakat_API_Manager
 */

if ( ! class_exists( 'Pakat_API_Manager' ) ) {
	/**
	 * Class Pakat_API_Manager.
	 * Main API class for pakat module.
	 */
	class Pakat_API_Manager {

		/** Transient delay time */
		const DELAYTIME = 900;

		/**
		 * Pakat_API_Manager constructor.
		 */
		function __construct() {

		}

		/** Get account info */
		public static function get_account_info() {
			// get account's info.
			$account_info = get_transient( 'pakat_credit_' . md5( Pakat_Manager::$access_key ) );
			if ( false === $account_info || false == $account_info ) {
				$client = new PakatApiClient();
                $account = $client->getAccount();
				if ($client->getLastResponseCode() === PakatApiClient::RESPONSE_CODE_OK && !empty($account['email'])) {
                    $account_email = $account['email'];

					$account_info = array(
						'account_email' => $account_email,
						'account_user_name' => $account['firstName'] . ' ' . $account['lastName'],
						'account_data' => $account['plan'],
					);
                    set_transient( 'pakat_credit_' . md5( Pakat_Manager::$access_key ), $account_info, self::DELAYTIME );
				} elseif ($client->getLastResponseCode() === PakatApiClient::RESPONSE_CODE_UNAUTHORIZED) {
				    delete_option(Pakat_Manager::API_KEY_V3_OPTION_NAME);
                }
			}
			return $account_info;
		}

		/** Get smtp status */
		public static function get_smtp_status() {
			$status = get_transient( 'pakat_smtp_status_' . md5( Pakat_Manager::$access_key ) );
			if ( false === $status || false == $status ) {
                $client = new PakatApiClient();
                $account = $client->getAccount();
				$status = 'disabled';
				if ($client->getLastResponseCode() == 200) {
					$status = $account['relay']['enabled'] ? 'enabled' : 'disabled';
					set_transient( 'pakat_smtp_status_' . md5( Pakat_Manager::$access_key ), $status, self::DELAYTIME );

					// get Marketing Automation API key.
					if ( isset( $account['marketingAutomation']['enabled'] ) && true == $account['marketingAutomation']['enabled'] ) {
						$ma_key = $account['marketingAutomation']['key'];
					} else {
						$ma_key = '';
					}
					$general_settings = get_option( Pakat_Manager::MAIN_OPTION_NAME, array() );
					$general_settings['ma_key'] = $ma_key;
					update_option( Pakat_Manager::MAIN_OPTION_NAME, $general_settings );
				}
			}
			return $status;
		}

		/** Get all attributes */
		public static function get_attributes() {
			// get attributes.
			$attrs = get_transient( 'pakat_attributes_' . md5( Pakat_Manager::$access_key ) );

			if ( false === $attrs || false == $attrs ) {
				$pakat_mail = new PakatApiClient();
				$response = $pakat_mail->getAttributes();
				$attributes = $response['attributes'];
				$attrs = array(
					'attributes' => array(
						'normal_attributes' => array(),
						'category_attributes' => array(),
					)
				);

				if ( count( $attributes ) > 0 ) {
					foreach ($attributes as $key => $value) {
						if ($value["category"] == "normal") {
							$attrs['attributes']['normal_attributes'][] = $value;
						}
						elseif ($value["category"] == "category") {
							$value["type"] = "category";
							$attrs['attributes']['category_attributes'][] = $value;
						}

					}
				}

				set_transient( 'pakat_attributes_' . md5( Pakat_Manager::$access_key ), $attrs, self::DELAYTIME );
			}

			return $attrs;

		}

		/** Get all smtp templates */
		public static function get_templates() {

			// get templates.
			$templates = get_transient( 'pakat_template_' . md5( Pakat_Manager::$access_key ) );

			if ( false === $templates || false == $templates ) {
				$pakat_mail = new PakatApiClient();
				$data = array(
					'templateStatus' => true
				);
				$templates = $pakat_mail->getEmailTemplates( $data );
				$template_data = array();

				if ( $pakat_mail->getLastResponseCode() === PakatApiClient::RESPONSE_CODE_OK ) {

					foreach ( $templates['templates'] as $template ) {
						$is_dopt = 0;
						if ( strpos( $template['htmlContent'], 'DOUBLEOPTIN' ) != false  || strpos( $template['htmlContent'], 'doubleoptin' ) != false) {
							$is_dopt = 1;
						}
						$template_data[] = array(
							'id' => $template['id'],
							'name' => $template['name'],
							'is_dopt' => $is_dopt,
						);

					}
				}
				$templates = $template_data;
				if ( count( $templates ) > 0 ) {
					set_transient( 'pakat_template_' . md5( Pakat_Manager::$access_key ), $templates, self::DELAYTIME );
				}
			}

			return $templates;
		}

		/** Get default list id after install */
		public static function get_default_list_id() {
			$lists = self::get_lists();
			return strval( $lists[0]['id'] );
		}

		/** Get all lists */
		public static function get_lists() {
			// get lists.
			$lists = get_transient( 'pakat_list_' . md5( Pakat_Manager::$access_key ) );
			if ( false === $lists || false == $lists ) {

				$pakat_mail = new PakatApiClient();
				$lists = array();
				$list_data = $pakat_mail->getAllLists();

				if (!empty($list_data['lists'])) {
                    foreach ( $list_data['lists'] as $value ) {
                        if ( 'Temp - DOUBLE OPTIN' == $value['name'] ) {
                            $tempList = $value['id'];
                            update_option( Pakat_Manager::TEMPLIST_OPTION_NAME, $tempList );
                            continue;
                        }
                        $lists[] = array(
                            'id' => $value['id'],
                            'name' => $value['name'],
                        );
                    }
                }
			}
			if ( count( $lists ) > 0 ) {
				set_transient( 'pakat_list_' . md5( Pakat_Manager::$access_key ), $lists, self::DELAYTIME );
			}
			return $lists;
		}

		/** Get all sender of pakat */
		public static function get_sender_lists() {
			$senders = get_transient( 'pakat_senders_' . md5( Pakat_Manager::$access_key ) );
			if ( false === $senders || false == $senders ) {
				$pakat_mail = new PakatApiClient();
				$response = $pakat_mail->getSenders();
				$senders = array();
				if ($pakat_mail->getLastResponseCode() === PakatApiClient::RESPONSE_CODE_OK) {
					// reorder by id.
					foreach ( $response['senders'] as $sender ) {
						$senders[] = array(
							'id' => $sender['id'],
							'from_name' => $sender['name'],
							'from_email' => $sender['email'],
						);
					}
				}
				if ( count( $senders ) > 0 ) {
					set_transient( 'pakat_senders_' . md5( Pakat_Manager::$access_key ), $senders, self::DELAYTIME );
				}
			}
			return $senders;
		}
		/** Remove all transients */
		public static function remove_transients() {
			// remove all transients.
			delete_transient( 'pakat_list_' . md5( Pakat_Manager::$access_key ) );
			delete_transient( 'pakat_totalusers_' . md5( Pakat_Manager::$access_key ) );
			delete_transient( 'pakat_credit_' . md5( Pakat_Manager::$access_key ) );
			delete_transient( 'pakat_campaigns_' . md5( Pakat_Manager::$access_key ) );
			delete_transient( 'pakat_smtp_status_' . md5( Pakat_Manager::$access_key ) );
			delete_transient( 'pakat_attributes_' . md5( Pakat_Manager::$access_key ) );
			delete_transient( 'pakat_template_' . md5( Pakat_Manager::$access_key ) );
			delete_transient( 'pakat_senders_' . md5( Pakat_Manager::$access_key ) );
		}

		/**
		 * Send Identify User for MA
		 *
		 * @param array $data - data.
		 */
		public static function identify_user( $data ) {
			$general_settings = get_option( Pakat_Manager::MAIN_OPTION_NAME, array() );
			if (isset($general_settings['ma_key'])) {
                try {
                    $event = new Pakat( $general_settings['ma_key'] );
                    $event->identify( $data );
                } catch (Exception $exception) {
                    echo $exception->getMessage() . "\n";
                }
            }
		}

		/**
		 * Send email through Pakat
		 *
		 * @param array $data - mail data.
		 * @return array|mixed|object
		 */
		public static function send_email( $data ) {
			$pakat_mail = new PakatApiClient( );
            try {
                if (isset($data['headers'])) {
                    $emailHeaders = $data['headers'];
                    unset($data['headers']);

                    if (!is_array($emailHeaders) && !is_string($emailHeaders)) {
                        return new WP_Error('email headers are not valid');
                    }

                    if (is_string($emailHeaders)) {
                        $emailHeaders = preg_split("/\r\n|\n|\r/", $emailHeaders);
                    }
                    $preparedHeaders = [];
                    foreach ($emailHeaders as $header) {
                        $header = explode(': ', $header);
                        if (is_array($header) && 2 == count($header)) {
                            if ($header[0] == 'X-Mailin-Tag') {
                                $data['tags'][] = $header[1];
                            }
                            $preparedHeaders[$header[0]] = $header[1];
                        }
                    }
                    $data['headers'] = $preparedHeaders;
                }
            } catch (Exception $exception) {
                return new WP_Error($exception->getMessage());
            }

            $home_options = get_option( Pakat_Manager::HOME_OPTION_NAME);
            if (!empty($home_options['from_email'])) {
                $data['sender']['email'] = $home_options['from_email'];
                if (!empty($home_options['from_name'])) {
                    $data['sender']['name'] = $home_options['from_name'];
                }
            }

			$result = $pakat_mail->sendEmail( $data );
            if (PakatApiClient::RESPONSE_CODE_CREATED == $pakat_mail->getLastResponseCode()) {
                return ['code' => 'success'];
            }

			return $result;
		}

        /**
         * Validation the email if it exist in contact list
         *
         * @param $res
         * @param string $type - form type.
         * @param string $email - email.
         * @param array $list_id - list ids.
         * @return array
         */
		static function validation_email( $res, $type = 'simple', $email, $list_id ) {

			$isDopted = false;

			$temp_dopt_list = get_option( Pakat_Manager::TEMPLIST_OPTION_NAME );

			$desired_lists = $list_id;

			if ( 'double-optin' == $type ) {
				$list_id = array( $temp_dopt_list );
			}

			// new user.
			if ( isset($res['code']) && $res['code'] == 'document_not_found' ) {
				$ret = array(
					'code' => 'new',
					'isDopted' => $isDopted,
					'listid' => $list_id,
				);
				return $ret;
			}

			$listid = $res['listIds'];

			// update user when listid is empty.
			if ( ! isset( $listid ) || ! is_array( $listid ) ) {
				$ret = array(
					'code' => 'update',
					'isDopted' => $isDopted,
					'listid' => $list_id,
				);
				return $ret;
			}

			$attrs = $res['attributes'];
			if ( isset( $attrs['DOUBLE_OPT-IN'] ) && '1' == $attrs['DOUBLE_OPT-IN'] ) {
				$isDopted = true;
			}
			// remove dopt temp list from $listid.
			if (($key = array_search($temp_dopt_list, $listid)) !== false) {
                unset($listid[$key]);
            }

			$diff = array_diff( $desired_lists, $listid );
			if ( ! empty( $diff ) ) {
				$status = 'update';
				if ( 'double-optin' != $type ) {
					$listid = array_unique( array_merge( $listid, $list_id ) );
				}
				if ( ( 'double-optin' == $type && ! $isDopted) ) {
					array_push( $listid, $temp_dopt_list );
				}
			} else {
				if ( '1' == $res['emailBlacklisted'] ) {
					$status = 'update';
				} else {
					$status = 'already_exist';
				}
			}

			$ret = array(
				'code' => $status,
				'isDopted' => $isDopted,
				'listid' => $listid,
			);
			return $ret;
		}

		/**
		 * Signup process
		 *
		 * @param string                     $type - simple, confirm, double-optin / subscribe.
		 * @param $email - subscriber email.
		 * @param $list_id - desired list ids.
		 * @param $info - user's attributes.
		 * @param null                       $list_unlink - remove temp list.
		 * @return string
		 */
		public static function create_subscriber( $type = 'simple', $email, $list_id, $info, $list_unlink = null ) {
            $pakat_mail = new PakatApiClient();
            $user = $pakat_mail->getUser($email);

			$response = self::validation_email( $user, $type, $email, $list_id );
			$exist = '';

			if ( 'already_exist' == $response['code'] ) {
				$exist = 'already_exist';
			}

			if ( 'subscribe' == $type ) {
				$info['DOUBLE_OPT-IN'] = '1'; // Yes.
			} else {
				if ( 'double-optin' == $type ) {
					if ( ( 'new' == $response['code'] && ! $response['isDopted']) || ( 'update' == $response['code'] && ! $response['isDopted']) ) {
						$info['DOUBLE_OPT-IN'] = '2'; // No.
					}
				}
			}

			$listid = $response['listid'];
            if ( $list_unlink != null ) {
                $listid = array_diff( $listid, $list_unlink );
            }

            $attributes = Pakat_API_Manager::get_attributes();
            if( !empty($attributes["attributes"]["normal_attributes"]) ) {
                foreach ( $attributes["attributes"]["normal_attributes"] as $key => $value ) {
                    if( "boolean" == $value["type"] && array_key_exists($value["name"], $info) )
                        if( in_array($info[ $value["name"] ], array("true","True","TRUE",1)) ) {
                            $info[ $value["name"] ] = true;
                        }
                        else {
                            $info[ $value["name"] ] = false;
						}
						if( "date" == $value["type"] && array_key_exists($value["name"], $info) ) {
							$date = $info[ $value["name"] ];
							$tempDate = explode('-', $date);
							$error = false;
							foreach ( $tempDate as $key => $val ) {
								if ( $val == "0" || $val == "00" || $val == "0000" ) {
									$error = true;
								}
							}
							if ( $error ) {
								wp_send_json(
                                    array(
                                        'status' => 'failure',
                                        'msg' => [
                                            'errorMsg' => 'Date format is invalid',
                                        ]
                                    )
                                );
							} else {
									try {
										$dateCheck = (new DateTime($date))->format('Y-m-d');
										$info[ $value["name"] ] =  $dateCheck;
									} catch (Exception $exception) {
										wp_send_json(
											array(
												'status' => 'failure',
												'msg' => [
													'errorMsg' => 'Date format is invalid',
												]
											)
										);
									}
							}
						}
                }
            }

			if ($pakat_mail->getLastResponseCode() === PakatApiClient::RESPONSE_CODE_OK && isset($user['email'])) {
                unset($info["email"]);
                if(!($type == 'double-optin')){
					$data = [
						'email' => $email,
						'attributes' => $info,
						'emailBlacklisted' => false,
						'smsBlacklisted' => false,
						'listIds' => $listid,
						'unlinkListIds' => $list_unlink
					];
				} else {
						if($info['DOUBLE_OPT-IN'] == '1'){
							$data = [
								'email' => $email,
								'attributes' => $info,
								'emailBlacklisted' => false,
								'smsBlacklisted' => false,
								'listIds' => $listid,
								'unlinkListIds' => $list_unlink
							];
						} else {
							$data = [
								'email' => $email,
								'attributes' => $info,
								'emailBlacklisted' => (($user["emailBlacklisted"] == '1') ? $user["emailBlacklisted"] : false),
								'smsBlacklisted' => false,
								'listIds' => $listid,
								'unlinkListIds' => $list_unlink
							];
						}
				}
                $pakat_mail->updateUser($email ,$data );
                $exist = $pakat_mail->getLastResponseCode() == 204 ? 'success' : '' ;
			} else {
                $info["internalUserHistory"] = array(array("action"=>"SUBSCRIBE_BY_PLUGIN", "id"=> 1, "name"=>"wordpress"));
				$data = [
                    'email' => $email,
                    'attributes' => $info,
                    'emailBlacklisted' => false,
                    'smsBlacklisted' => false,
                    'listIds' => $listid
                ];

				$created_user = $pakat_mail->createUser( $data );
			}

			if ('' !=  $exist) {
				$response['code'] = $exist;
			} else if(isset($created_user['id'])) {
				$response['code'] = "success";
			}

			return $response['code'];
		}

		/**
		 * Send a mail for confirmation through Pakat
		 *
		 * @param string                   $type - confirm or double-optin.
		 * @param $to_email - receive email.
		 * @param string                   $template_id - template id.
		 * @param null                     $attributes - attributes.
		 * @param string                   $code - code.
		 */
		public static function send_comfirm_email( $type = 'confirm', $to_email, $template_id = '-1', $attributes = null, $code = '' ) {
			$pakat_mail = new PakatApiClient();

			// set subject info.
			if ( 'confirm' == $type ) {
				$subject = __( 'Subscription confirmed', 'pakat_lang' );
			} elseif ( 'double-optin' == $type ) {
				$subject = __( 'Please confirm subscription', 'pakat_lang' );
			}

			// get sender info.
			$home_settings = get_option( Pakat_Manager::HOME_OPTION_NAME );
			if ( isset( $home_settings['sender'] ) ) {
				$sender_name = $home_settings['from_name'];
				$sender_email = $home_settings['from_email'];
			} else {
				$sender_email = trim( get_bloginfo( 'admin_email' ) );
				$sender_name = trim( get_bloginfo( 'name' ) );
			}
			if ( '' == $sender_email ) {
				$sender_email = __( 'no-reply@pakat.net', 'pakat_lang' );
				$sender_name = __( 'Pakat', 'pakat_lang' );
			}

			$template_contents = self::get_email_template( $type );
			$html_content = $template_contents['html_content'];

			$transactional_tags = 'WordPress Mailin';
			$attachment = array();

			// get info from Pakat template.
			if ( 'yes' == $home_settings['activate_email'] && intval( $template_id ) > 0 && ( 'confirm' == $type ) ) {
				$data = array(
					'replyTo' => array('email' => $sender_email),
					'to' => array(array('email' => $to_email)),
				);
				$data["templateId"] = intval( $template_id );
				$pakat_mail->sendEmail( $data );
				return;
			}
			else if ( intval( $template_id ) > 0 ) {
				$data = array(
					'id' => $template_id,
				);
				$response = $pakat_mail->getEmailTemplate( $data["id"] );
				if ( $pakat_mail->getLastResponseCode() === PakatApiClient::RESPONSE_CODE_OK ) {
					$html_content = $response['htmlContent'];
					if ( trim( $response['subject'] ) != '' ) {
						$subject = trim( $response['subject'] );
					}
					if ( ( '[DEFAULT_FROM_NAME]' != $response['sender']['name'] ) &&
						( '[DEFAULT_FROM_EMAIL]' != $response['sender']['email'] ) &&
						( '' != $response['sender']['email'] )
					) {
						$sender_name = $response['sender']['name'];
						$sender_email = $response['sender']['email'];
					}
					$transactional_tags = $response['sender']['name'];

					// pls ask Ekta about attachment of template.
				}
			}

			// send mail.
			$to = array(
				$to_email => '',
			);
			$from = array( $sender_email, $sender_name );

			$site_domain = str_replace( 'https://', '', home_url() );
			$site_domain = str_replace( 'http://', '', $site_domain );

			$html_content = str_replace( '{title}', $subject, $html_content );

			$html_content = str_replace( '{site_domain}', $site_domain, $html_content );
			$encodedEmail = rtrim( strtr( base64_encode( $to_email ), '+/', '-_' ), '=' );
			$search_value = "({{\s*doubleoptin\s*}})";

			// double optin
			$html_content = str_replace( 'https://[DOUBLEOPTIN]', '{subscribe_url}', $html_content );
			$html_content = str_replace( 'http://[DOUBLEOPTIN]', '{subscribe_url}', $html_content );
			$html_content = str_replace( 'https://{{doubleoptin}}', '{subscribe_url}', $html_content );
			$html_content = str_replace( 'http://{{doubleoptin}}', '{subscribe_url}', $html_content );
			$html_content = str_replace( 'https://{{ doubleoptin }}', '{subscribe_url}', $html_content );
			$html_content = str_replace( 'http://{{ doubleoptin }}', '{subscribe_url}', $html_content );
			$html_content = str_replace( '[DOUBLEOPTIN]', '{subscribe_url}', $html_content );
			$html_content = preg_replace($search_value, '{subscribe_url}', $html_content);
			$html_content = str_replace(
				'{subscribe_url}', add_query_arg(
					array(
						'pakat_action' => 'subscribe',
						'code' => $code,
					), home_url()
				), $html_content
			);

			if ( 'yes' == $home_settings['activate_email'] ) {

				$data = array(
					'replyTo' => array('email' => $from[0]),
					'to' => array(array('email' => $to_email)),
				);
				$data['sender'] = [ 'email' => $from[0], 'name' => $from[1] ];
				$data['htmlContent'] = $html_content;
				$data['subject'] = $subject;

				$res = $pakat_mail->sendEmail( $data );

			} else {
				$headers[] = 'Content-Type: text/html; charset=UTF-8';
				$headers[] = "From: $sender_name <$sender_email>";
				@wp_mail( $to_email, $subject, $html_content, $headers );
			}
		}

		/**
		 * Get email template by type (test, confirmation, double-optin).
		 *
		 * @param string $type - email template type.
		 * @return array
		 */
		static function get_email_template( $type = 'test' ) {
			$lang = get_bloginfo( 'language' );
			if ( 'fr-FR' == $lang ) {
				$file = 'temp_fr-FR';
			} else {
				$file = 'temp';
			}

			$file_path = Pakat_Manager::$plugin_dir . '/inc/templates/' . $type . '/';
			// get html content.
			$html_content = file_get_contents( $file_path . $file . '.html' );
			// get text content.
			$text_content = file_get_contents( $file_path . $file . '.txt' );
			$templates = array(
				'html_content' => $html_content,
				'text_content' => $text_content,
			);
			return $templates;
		}

		/**
		 * Sync wp users to contact list.
		 *
		 * @param string $users_info - user's attributes.
		 * @param array $list_ids - desired lists
		 * @return array|mixed|object
		 */
		public static function sync_users( $users_info, $list_ids ) {
			$client = new PakatApiClient();
			$data = array(
				'fileBody' => $users_info,
				'listIds' => $list_ids,
			);
			$client->importContacts($data);
			if (  PakatApiClient::RESPONSE_CODE_ACCEPTED == $client->getLastResponseCode() ) {
                $response = array(
                    'code' => 'success',
                    'message' => __( 'Contact synchronization has started.', 'pakat_lang' )
                );
            } else {
                $response = array(
                    'code' => 'failed',
                    'message' => __( 'Something went wrong. PLease try again.', 'pakat_lang' )
                );
            }
			return $response;
		}

		/**
		 * Subscribe process for double optin subscribers
		 */
		public static function subscribe() {
			$code = isset( $_GET['code'] ) ? sanitize_text_field( $_GET['code'] ) : '';

			$contact_info = Pakat_Model_Users::get_data_by_code( $code );

			if ( false != $contact_info ) {
				$email = $contact_info['email'];
				$info = maybe_unserialize( $contact_info['info'] );
				$list_id = maybe_unserialize( $contact_info['listIDs'] );
                $form_id = $contact_info['frmid'];
                $current_form = Pakat_Forms::getForm( $form_id );
                $unlinkedLists = null;
                if( isset( $info['unlinkedLists'] ) )
                {
                    $unlinkedLists = $info['unlinkedLists'];
                    unset($info['unlinkedLists']);
                }
                if ( '1' == $current_form['isDopt'] )
                {
                    Pakat_API_Manager::send_comfirm_email( 'confirm', $email, $current_form['confirmID'], $info );
                }
				// temp dopt list.
				$temp_list = get_option( Pakat_Manager::TEMPLIST_OPTION_NAME );
                if( $unlinkedLists != null ) {
                    $unlinkedLists[] = $temp_list;
                    self::create_subscriber( 'subscribe', $email, $list_id, $info, $unlinkedLists );
                }
                else {
                    self::create_subscriber( 'subscribe', $email, $list_id, $info, array( $temp_list ) );
                }

				// remove the record.
				$id = $contact_info['id'];
				Pakat_Model_Users::remove_record( $id );
			}

			if ( '' != $contact_info['redirectUrl'] ) {
                wp_redirect( $contact_info['redirectUrl'] );
				exit;
			}

			$site_domain = str_replace( 'https://', '', home_url() );
			$site_domain = str_replace( 'http://', '', $site_domain );
			?>
			<body style="margin:0; padding:0;">
			<table style="background-color:#ffffff" cellpadding="0" cellspacing="0" border="0" width="100%">
				<tbody>
				<tr style="border-collapse:collapse;">
					<td style="border-collapse:collapse;" align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td style="line-height:0; font-size:0;" height="20"></td>
							</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td style="line-height:0; font-size:0;" height="20">
									<div
										style="font-family:arial,sans-serif; color:#61a6f3; font-size:20px; font-weight:bold; line-height:28px;">
										<?php esc_attr_e( 'Thank you for subscribing', 'pakat_lang' ); ?></div>
								</td>
							</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td style="line-height:0; font-size:0;" height="20"></td>
							</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td align="left">

									<div
										style="font-family:arial,sans-serif; font-size:14px; margin:0; line-height:24px; color:#555555;">
										<br>
										<?php echo esc_attr__( 'You have just subscribed to the newsletter of ', 'pakat_lang' ) . esc_attr( $site_domain ) . ' .'; ?>
										<br><br>
										<?php esc_attr_e( '-Pakat', 'pakat_lang' ); ?></div>
								</td>
							</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td style="line-height:0; font-size:0;" height="20">
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
			</body>
			<?php
			exit;
		}

		/**
		 * Unsubscribe process
		 */
		function unsubscribe() {
			$pakat_mail = new PakatApiClient();
			$code = isset( $_GET['code'] ) ? esc_attr( $_GET['code'] ) : '' ;
			$list_id = isset( $_GET['li'] ) ? intval( esc_attr( $_GET['li'] ) ) : '' ;

			$email = base64_decode( strtr( $code, '-_', '+/' ) );
			$data = array(
				'email' => $email,
			);
			$response = $pakat_mail->get_user( $data );

			if ($pakat_mail->getLastResponseCode() === PakatApiClient::RESPONSE_CODE_OK) {
				$attributes = $response['attributes'];

				$listid = $response['listIds'];

				$blacklisted = $response['emailBlacklisted'];
				$diff_listid = array_diff( $listid, array( $list_id ) );

				if ( count( $diff_listid ) == 0 ) {
					$blacklisted = true;
					$diff_listid = $listid;
				}
				$data = array(
					'email' => $email,
					'data' =>'{"listIds":'.$diff_listid.',"emailBlacklisted":'.$blacklisted.'}'
				);
				$pakat_mail->updateUser( $data["email"],$data["data"] );
			}
			?>
			<body style="margin:0; padding:0;">
			<table style="background-color:#ffffff" cellpadding="0" cellspacing="0" border="0" width="100%">
				<tbody>
				<tr style="border-collapse:collapse;">
					<td style="border-collapse:collapse;" align="center">
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td style="line-height:0; font-size:0;" height="20"></td>
							</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td style="line-height:0; font-size:0;" height="20">
									<div
										style="font-family:arial,sans-serif; color:#61a6f3; font-size:20px; font-weight:bold; line-height:28px;">
										<?php esc_attr_e( 'Unsubscribe', 'pakat_lang' ); ?></div>
								</td>
							</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td style="line-height:0; font-size:0;" height="20"></td>
							</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td align="left">

									<div
										style="font-family:arial,sans-serif; font-size:14px; margin:0; line-height:24px; color:#555555;">
										<br>
										<?php esc_attr_e( 'Your request has been taken into account.', 'pakat_lang' ); ?><br>
										<br>
										<?php esc_attr_e( 'The user has been unsubscribed', 'pakat_lang' ); ?><br>
										<br>
										-Pakat
									</div>
								</td>
							</tr>
							</tbody>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="540">
							<tbody>
							<tr>
								<td style="line-height:0; font-size:0;" height="20">
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
			</body>
			<?php
			exit;
		}

		/** Create list and attribute for double optin */
		public static function create_default_dopt() {

			$pakat_mail = new PakatApiClient();

			// get folder id
			$folder_data = $pakat_mail->getAllFolders();
			foreach ( $folder_data['folders'] as $value ) {
				if ( 'FORM' == $value['name'] ) {
					$formFolderId = $value['id'];
					break;
				}
			}
			// create folder if not exists
			if ( empty( $formFolderId ) ){
				$data = ["name"=> "FORM"];
				$folderCreated = $pakat_mail->createFolder($data);
				$formFolderId = $folderCreated['id'];
			}

			// add list.
			$isEmpty = false;

			$list_data = $pakat_mail->getAllLists();
			foreach ( $list_data['lists'] as $value ) {
				if ( 'Temp - DOUBLE OPTIN' == $value['name'] ) {
					$isEmpty = true;
					break;
				}
			}

			if(!$isEmpty) {
				$data = array(
					'name' => 'Temp - DOUBLE OPTIN',
					'folderId' => $formFolderId,
				);
				$pakat_mail->createList( $data );
			}


			// add attribute.
			$isEmpty = false;
			$ret = $pakat_mail->getAttributes();

            if (isset($ret["attributes"])) {
                foreach ($ret["attributes"] as $key => $value) {
                    if($value["category"] == "category" && 'DOUBLE_OPT-IN' == $value['name'] && ! empty( $value['enumeration'] ) ) {
                        $isEmpty = true;
                    }
                }

                if ( ! $isEmpty ) {
                    $data = [
                        'type' => 'category',
                        'enumeration' => [
                            [
                                'value' => 1,
                                'label' => 'Yes'
                            ],
                            [
                                'value' => 2,
                                'label' => 'No'
                            ],
                        ]
                    ];
                    $pakat_mail->createAttribute('category', 'DOUBLE_OPT-IN', $data);
                }
            }
		}
	}
}
