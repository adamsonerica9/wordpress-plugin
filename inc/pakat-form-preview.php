<?php
/**
 * Page to preview form
 */

$pakat_form_id = isset($_GET['pakat_form']) ? esc_attr($_GET['pakat_form']) : '';
$pakat_preview = isset($_GET['action']) ? esc_attr($_GET['action']) : '';

wp_head();

?>
<body style="background-color: #f5f5f5;">
    <div id="page" class="site" style="padding:16px;">
        <div id="pakat-preview-form">
        <?php
        if($pakat_preview == '') {
            $formData = Pakat_Forms::getForm($pakat_form_id);
        } else {
            $formData = get_option(Pakat_Manager::PREVIEW_OPTION_NAME, array());
        }
        if( isset( $formData['gCaptcha'] ) && '0' != $formData['gCaptcha'] ) {
            if( '1' == $formData['gCaptcha'] ) {   // For old forms.
                $formData['html'] = preg_replace( '/([\s\S]*?)<div class="g-recaptcha"[\s\S]*?data-size="invipakatle"><\/div>/', '$1', $formData['html'] );
            }
            if ( '3' == $formData['gCaptcha'] ) {     // The case of using google recaptcha.
                ?>
                <script type="text/javascript">
                    var onloadPakatCallback = function() {
                        grecaptcha.render('pakat_captcha',{
                            'sitekey' : '<?php echo $formData["gCaptcha_site"] ?>'
                        });
                    };
                </script>
            <?php
            }
            else {                                  // The case of using google invipakatle recaptcha.
            ?>
                <script type="text/javascript">
                    var onloadPakatCallback = function() {
                        var element = document.getElementsByClassName('pakat-default-btn');
                        grecaptcha.render(element[0],{
                            'sitekey' : '<?php echo $formData["gCaptcha_site"] ?>',
                            'callback' : pakatVerifyCallback
                        });
                    };
                </script>
            <?php
            }
            ?>
            <script src="https://www.google.com/recaptcha/api.js?onload=onloadPakatCallback&render=explicit" async defer></script>
            <?php
        }

        $html = stripslashes_deep($formData['html']);
        $css = stripslashes_deep($formData['css']);
        echo $html;
        ?>
        </div>
        <style>
            <?php
                if($formData['dependTheme'] != '1'){
                    $css = str_replace('[form]', '#pakat-preview-form', $css);
                    echo $css;
                }
            ?>
        </style>
    </div>
</body>
