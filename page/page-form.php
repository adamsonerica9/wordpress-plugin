<?php
/**
 * Admin page : dashboard
 *
 * @package Pakat_Page_Form
 */

if ( ! class_exists( 'Pakat_Page_Form' ) ) {
	/**
	 * Page class that handles backend page <i>dashboard ( for admin )</i> with form generation and processing
	 *
	 * @package Pakat_Page_Form
	 */
	class Pakat_Page_Form {
		/** Page slug */
		const PAGE_ID = 'pakat_page_form';

		/**
		 * Page hook
		 *
		 * @var false|string
		 */
		protected $page_hook;

		/**
		 * Page tabs
		 *
		 * @var mixed
		 */
		protected $tabs;

		/**
		 * Form ID
		 *
		 * @var string
		 */
		public $formID;

        /**
         * Default compliant Note
         *
         * @var string
         */
		public $defaultComplianceNote;

		/**
		 * Constructs new page object and adds entry to WordPress admin menu
		 */
		function __construct() {
		    $title = get_bloginfo('name');
		    $this->defaultComplianceNote = sprintf( esc_attr('Your e-mail address is only used to send you our newsletter and information about the activities of %s. You can always use the unsubscribe link included in the newsletter.', 'pakat_lang'), $title);
			$this->page_hook = add_submenu_page( Pakat_Page_Home::PAGE_ID, __( 'Forms', 'pakat_lang' ), __( 'Forms', 'pakat_lang' ), 'manage_options', self::PAGE_ID, array( &$this, 'generate' ) );
			add_action( 'admin_print_scripts-' . $this->page_hook, array( $this, 'enqueue_scripts' ) );
			add_action( 'admin_print_styles-' . $this->page_hook, array( $this, 'enqueue_styles' ) );
			add_action( 'load-' . $this->page_hook, array( &$this, 'init' ) );
		}

		/**
		 * Init Process
		 */
		function Init() {
            Pakat_Manager::is_done_validation();
            $this->forms = new Pakat_Forms_List();
            $this->forms->prepare_items();
		}

		/**
		 * Enqueue scripts of plugin
		 */
		function enqueue_scripts() {
			wp_enqueue_script( 'pakat-admin-js' );
			wp_enqueue_script( 'pakat-bootstrap-js' );
			wp_enqueue_script( 'pakat-chosen-js' );

			wp_localize_script(
				'pakat-admin-js', 'ajax_pakat_object',
				array(
					'ajax_url' => admin_url( 'admin-ajax.php' ),
                    'ajax_nonce' => wp_create_nonce( 'ajax_pakat_admin_nonce' ),
                    'compliance_note' => $this->defaultComplianceNote
				)
			);
			wp_localize_script( 'pakat-admin-js', 'pakat_img_url', array(Pakat_Manager::$plugin_url.'/img/flags/') );
		}

		/**
		 * Enqueue style sheets of plugin
		 */
		function enqueue_styles() {
			wp_enqueue_style( 'pakat-admin-css' );
			wp_enqueue_style( 'pakat-bootstrap-css' );
			wp_enqueue_style( 'pakat-chosen-css' );
			wp_enqueue_style( 'pakat-fontawesome-css' );
			wp_enqueue_style( 'thickbox' );
		}

		/** Generate page script */
		function generate() {
			?>
			<div id="wrap" class="wrap box-border-box container-fluid">
				<h1><img id="logo-img" src="<?php echo esc_url( Pakat_Manager::$plugin_url . '/img/logo.png' ); ?>">
					<?php
					$return_btn = 'none';
					if (isset( $_GET['id'] ) ) {
						$return_btn = 'inline-block';
					}
					?>
				<a href="<?php echo esc_url( add_query_arg( 'page', self::PAGE_ID, admin_url( 'admin.php' ) ) ); ?>" class="button" style="margin-top: 6px; display: <?php echo esc_attr( $return_btn ); ?>;"><?php esc_attr_e( 'Back to form\'s list' ,'pakat_lang' ); ?></a>
				</h1>
				<div id="wrap-left" class="box-border-box col-md-9 ">
					<input type="hidden" class="pakat-dateformat" value="<?php echo esc_attr( 'yyyy-mm-dd' ); ?>">
					<?php
					if ( Pakat_Manager::is_api_key_set() ) {
						if ( ( isset( $_GET['action'] ) && 'edit' === sanitize_text_field($_GET['action'] )) || ( isset( $_GET['action'] ) && 'duplicate' === sanitize_text_field($_GET['action'] )) ) {
							$this->formID = isset( $_GET['id'] ) ? sanitize_text_field( $_GET['id'] ) : 'new';
							$this->generate_form_edit();
						} else {
							$this->generate_forms_page();
						}
					} else {
						$this->generate_welcome_page();
					}
					?>
				</div>
				<div id="wrap-right-side" class="box-border-box col-md-3">
					<?php

					Pakat_Page_Home::generate_side_bar();
					?>
				</div>
			</div>
			<div id="pakat_modal" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4><?php esc_attr_e( 'You are about to change the language', 'pakat_lang' ); ?></h4>
						</div>
						<div class="modal-body">
							<p><?php esc_attr_e( "Please make sure that you've saved all the changes. We will have to reload the page.", 'pakat_lang' ); ?></p>
							<p><?php esc_attr_e( 'Do you want to continue?', 'pakat_lang' ); ?></p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" id="pakat_modal_ok"><?php esc_attr_e( 'Ok', 'pakat_lang' ); ?></button>
							<button type="button" class="btn btn-default" id="pakat_modal_cancel"><?php esc_attr_e( 'Cancel', 'pakat_lang' ); ?></button>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		/** Generate forms page */
		function generate_forms_page() {
			?>
			<div id="main-content" class="pakat-content">
				<div class="panel panel-default row pakat-small-content">
				<div class="page-header"><strong><?php esc_attr_e( 'Forms', 'pakat_lang' ); ?></strong></div>

					<form method="post" class="pakat-forms-wrapper" style="padding:20px;min-height: 500px;">
						<i style="font-size: 13px;"><?php esc_attr_e( "Note: Forms created in Pakat plugin for WordPress won't be displayed in Forms section in Pakat application", 'pakat_lang' ); ?></i>
			<?php
			$this->forms->display();
			?>
					</form>
				</div>
			</div>
			<?php
		}
		/** Generate form edit page */
		function generate_form_edit() {
			$is_activated_smtp = Pakat_API_Manager::get_smtp_status() == 'disabled' ? 0 : 1;
			$formData = Pakat_Forms::getForm( $this->formID );
			$invipakatleCaptcha = '1';
			if ( ! empty( $formData ) ) {
				if ( isset( $_GET['action'] ) && 'duplicate' === esc_attr($_GET['action']) ) {
					$this->formID = 'new';
					$formData['title'] = '';
				}
				if ( 'new' === $this->formID && isset( $_GET['pid'] ) ) {
					$parent_formData = Pakat_Forms::getForm( sanitize_text_field( $_GET['pid'] ) );
					$formData['title'] = $parent_formData['title'];
				}
				if ( ! isset( $formData['gCaptcha'] ) ) {
					$gCaptcha = '0';
				}
				else {
					if( '0' == $formData['gCaptcha'] ) {
						$gCaptcha = '0';
					}
					else {
						$gCaptcha = '1';
					}
					if ( '3' == $formData['gCaptcha'] ) {
						$invipakatleCaptcha = '0';
					}
					else {
						$invipakatleCaptcha = '1';
					}
				}
				if ( ! isset( $formData['termAccept'] ) ) {
					$formData['termAccept'] = '0';
				}

				?>
				<div id="main-content" class="pakat-content">
					<form action="admin.php" class="" method="post" role="form">
						<input type="hidden" name="action" value="pakat_setting_subscription">
						<input type="hidden" name="pakat_form_id" value="<?php echo esc_attr( $this->formID ); ?>">
						<input type="hidden" id="is_smtp_activated" value="<?php echo esc_attr( $is_activated_smtp ); ?>">
						<?php
						if ( isset( $_GET['pid'] ) ) {
							?>
							<input type="hidden" name="pid" value="<?php echo sanitize_text_field( $_GET['pid'] ); ?>">
							<?php if ( isset( $_GET['lang'] ) ) { ?>
								<input type="hidden" name="lang" value="<?php echo sanitize_text_field( $_GET['lang'] ); ?>">
							<?php
}
						}
						?>
						<?php wp_nonce_field( 'pakat_setting_subscription' ); ?>
						<!-- Subscription form -->
						<div class="panel panel-default row pakat-small-content">
							<div class="page-header">
								<strong><?php esc_attr_e( 'Subscription form', 'pakat_lang' ); ?></strong>&nbsp;<i
									id="pakat_setting_form_spin" class="fa fa-spinner fa-spin fa-fw fa-lg fa-2x"></i>
							</div>
							<div id="pakat_setting_form_body" class="panel-body">
								<div class="row <!--small-content-->">
									<div style="margin: 12px 20px 34px 20px;">
										<b><?php esc_attr_e( 'Form Name : ', 'pakat_lang' ); ?></b>&nbsp; <input type="text"
																									 name="pakat_form_name"
																									 value="<?php echo esc_attr( $formData['title'] ); ?>"
																									 style="width: 60%;"
																									 required="required"/>
									</div>
									<div class="col-md-6">

										<?php
										if ( function_exists( 'wp_editor' ) ) {
											wp_editor(
												esc_html(stripcslashes($formData['html'])), 'pakatformmarkup', array(
													'tinymce' => false,
													'media_buttons' => true,
													'textarea_name' => 'pakat_form_html',
													'textarea_rows' => 15,
												)
											);
										} else {
											?>
											<textarea class="widefat" cols="160" rows="20" id="pakatformmarkup"
														name="pakat_form_html"><?php echo stripcslashes( $formData['html'] ); ?></textarea>
																						<?php
										}
										?>
										<br>

										<p>
											<?php
											esc_attr_e( 'Use the shortcode', 'pakat_lang' );
											if ( isset( $_GET['pid'] ) ) {
												$id = sanitize_text_field( $_GET['pid'] );
											} else {
												$id = 'new' !== $this->formID ? $this->formID : '';
											}
											?>
											<i style="background-color: #eee;padding: 3px;">[pakatwp_form
												id=<?php echo esc_attr( $id ); ?>]</i>
											<?php
											esc_attr_e( 'inside a post, page or text widget to display your sign-up form.', 'pakat_lang' );
											?>
											<b><?php esc_attr_e( 'Do not copy and paste the above form mark up, that will not work', 'pakat_lang' ); ?></b>
										</p>
										<div id="pakat-field-form" class="panel panel-default row form-field"
											 style="padding-bottom: 20px;">

											<div class="row small-content2"
												 style="margin-top: 15px;margin-bottom: 15px;">
												<b><?php esc_attr_e( 'Add a new Field', 'pakat_lang' ); ?></b>&nbsp;
												<?php Pakat_Page_Home::get_narration_script( __( 'Add a New Field', 'pakat_lang' ), __( 'Choose an attribute and add it to the subscription form of your Website', 'pakat_lang' ) ); ?>
											</div>
											<div id="pakat_sel_attribute_area" class="row small-content2"
												 style="margin-top: 20px;">
											</div>
											<div id="pakat-field-content">
												<div style="margin-top: 30px;">
													<div class="pakat-attr-normal pakat-attr-category row small-content2"
														 style="margin-top: 10px;" id="pakat_field_label_area">
														<?php esc_attr_e( 'Label', 'pakat_lang' ); ?>
														<small>(<?php esc_attr_e( 'Optional', 'pakat_lang' ); ?>)</small>
														<input type="text" class="col-md-12 pakat_field_changes" id="pakat_field_label">
													</div>
													<div class="pakat-attr-normal row small-content2"
														 style="margin-top: 10px;" id="pakat_field_placeholder_area">
														<span><?php esc_attr_e( 'Place holder', 'pakat_lang' ); ?>
															<small>(<?php esc_attr_e( 'Optional', 'pakat_lang' ); ?>)
															</small> </span>
														<input type="text" class="col-md-12 pakat_field_changes" id="pakat_field_placeholder">
													</div>
													<div class="pakat-attr-normal row small-content2"
														 style="margin-top: 10px;" id="pakat_field_initial_area">
														<span><?php esc_attr_e( 'Initial value', 'pakat_lang' ); ?>
															<small>(<?php esc_attr_e( 'Optional', 'pakat_lang' ); ?>)
															</small> </span>
														<input type="text" class="col-md-12 pakat_field_changes" id="pakat_field_initial">
													</div>
													<div class="pakat-attr-other row small-content2"
														 style="margin-top: 10px;" id="pakat_field_button_text_area">
														<span><?php esc_attr_e( 'Button Text', 'pakat_lang' ); ?></span>
														<input type="text" class="col-md-12 pakat_field_changes" id="pakat_field_button_text">
													</div>
												</div>
												<div style="margin-top: 20px;">

													<div class="pakat-attr-normal pakat-attr-category row small-content2" style="margin-top: 5px;" id="pakat_field_required_area">
														<label style="font-weight: normal;"><input type="checkbox" class="pakat_field_changes" id="pakat_field_required">&nbsp;&nbsp;<?php esc_attr_e( 'Required field ?', 'pakat_lang' ); ?>
														</label>
													</div>
													<div class="pakat-attr-category row small-content2"
														 style="margin-top: 5px;" id="pakat_field_type_area">
														<label style="font-weight: normal;"><input type="radio" class="pakat_field_changes" name="pakat_field_type" value="select"
																								   checked>&nbsp;<?php esc_attr_e( 'Drop-down List', 'pakat_lang' ); ?>
														</label>&nbsp;&nbsp;
														<label style="font-weight: normal;"><input type="radio" class="pakat_field_changes" name="pakat_field_type"
																								   value="radio">&nbsp;<?php esc_attr_e( 'Radio List', 'pakat_lang' ); ?>
														</label>
													</div>
												</div>
												<div class="row small-content2" style="margin-top: 20px;"
													 id="pakat_field_add_area">
													<button type="button" id="pakat_add_to_form_btn"
															class="btn btn-default pakat-add-to-form"><span
															class="pakat-large-icon"><</span> <?php esc_attr_e( 'Add to form', 'pakat_lang' ); ?>
													</button>&nbsp;&nbsp;
													<?php Pakat_Page_Home::get_narration_script( __( 'Add to form', 'pakat_lang' ), __( 'Please click where you want to insert the field and click on this button. By default, the new field will be added at top.', 'pakat_lang' ) ); ?>
												</div>
												<div class="row small-content2" style="margin-top: 20px;"
													 id="pakat_field_html_area">
													<span><?php esc_attr_e( 'Generated HTML', 'pakat_lang' ); ?></span>
													<textarea class="col-md-12" style="height: 140px;"
															  id="pakat_field_html"></textarea>
												</div>
											</div>
										</div>
                                        <!---- multi list per interest ----->
                                        <div id="pakat-multi-lists" class="panel panel-default row form-field"
                                             style="padding-bottom: 20px;">

                                            <div class="row small-content2"
                                                 style="margin-top: 15px;margin-bottom: 15px;">
                                                <b><?php esc_attr_e( 'Add Multi-List Subscription', 'pakat_lang' ); ?></b>&nbsp;
                                                <?php Pakat_Page_Home::get_narration_script( __( 'Add Multi-List Subscription', 'pakat_lang' ), __( 'Enable your contacts to subscribe to content based on specific interests or preferences. Create a contact list for each interest and allow them to subscribe using this field', 'pakat_lang' ) ); ?>
                                            </div>
                                            <div id="pakat_sel_multi_list_area" class="row small-content2"
                                                 style="margin-top: 20px;">
                                                <input type="hidden" id="pakat_selected_multi_list_id" value="">
                                                <select data-placeholder="<?php esc_attr_e( 'Please select the lists', 'pakat_lang' ); ?>" id="pakat_select_multi_list"
                                                        class="col-md-12 chosen-select" name="multi_list_ids[]" multiple=""
                                                        tabindex="-1"></select>
                                            </div>
                                            <div id="pakat_multi_list_field" style="display: none;">
                                                <div style="margin-top: 30px;">
                                                    <div class="pakat-attr-normal pakat-attr-category row small-content2"
                                                         style="margin-top: 10px;" id="pakat_multi_field_label_area">
                                                        <?php esc_attr_e( 'Label', 'pakat_lang' ); ?>
                                                        <small>(<?php esc_attr_e( 'Optional', 'pakat_lang' ); ?>)</small>
                                                        <input type="text" class="col-md-12 pakat_field_changes" id="pakat_multi_field_label">
                                                    </div>
                                                </div>
                                                <div style="margin-top: 20px;">
                                                    <div class="pakat-attr-normal pakat-attr-category row small-content2" style="margin-top: 5px;" id="pakat_multi_field_required_area">
                                                        <label style="font-weight: normal;"><input type="checkbox" class="pakat_field_changes" id="pakat_multi_field_required">&nbsp;&nbsp;<?php esc_attr_e( 'Required field ?', 'pakat_lang' ); ?>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row small-content2" style="margin-top: 20px;"
                                                     id="pakat_multi_field_add_area">
                                                    <button type="button" id="pakat_multi_lists_add_form_btn"
                                                            class="btn btn-default pakat-add-to-form"><span
                                                                class="pakat-large-icon"><</span> <?php esc_attr_e( 'Add to form', 'pakat_lang' ); ?>
                                                    </button>&nbsp;&nbsp;
                                                    <?php Pakat_Page_Home::get_narration_script( __( 'Add to form', 'pakat_lang' ), __( 'Please click where you want to insert the field and click on this button. By default, the new field will be added at top.', 'pakat_lang' ) ); ?>
                                                </div>
                                                <div class="row small-content2" style="margin-top: 20px;"
                                                     id="pakat_field_html_area">
                                                    <span><?php esc_attr_e( 'Generated HTML', 'pakat_lang' ); ?></span>
                                                    <textarea class="col-md-12" style="height: 140px;"
                                                              id="pakat_multi_field_html"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="pakat-gdpr-block" class="panel panel-default row form-field" style="padding-bottom: 20px;display: none">
                                            <div class="row small-content2"
                                                 style="margin-top: 15px;margin-bottom: 15px;">
                                                <b><?php esc_attr_e( 'Compliance Note', 'pakat_lang' ); ?></b>&nbsp;
                                                <?php Pakat_Page_Home::get_narration_script( __( 'Add compliance note', 'pakat_lang' ), __( 'Create GDPR-compliant subscription forms for collecting email addresses.', 'pakat_lang' ) ); ?>
                                            </div>
                                            <div class="row small-content2" style="margin-top: 0px;">
                                                <input type="radio" name="pakat_add_compliant_note" class="pakat-add-compliant-note" value="1" ><label class="pakat-radio-label">&nbsp;<?php esc_attr_e( 'Yes', 'pakat_lang');?></label>
                                                <input type="radio" name="pakat_add_compliant_note" class="pakat-add-compliant-note" value="0" checked><label class="pakat-radio-label">&nbsp;<?php esc_attr_e( 'No', 'pakat_lang');?></label>
                                            </div>
                                            <div class="row small-content2 pakat-gdpr-block-area" style="display: none;">
                                                <textarea id="pakat_gdpr_text" class="col-md-12" rows="5"><?php echo trim( $this->defaultComplianceNote ); ?></textarea>
                                                <label id="set_gdpr_default"><?php esc_attr_e('Reset to Default', 'pakat_lang');?>&nbsp;<i class="fa fa-refresh"></i></label>
                                            </div>
                                            <div class="row small-content2 pakat-gdpr-block-btn" style="display: none;">
                                                <button type="button" id="pakat_add_compliance_note"
                                                        class="btn btn-default pakat-add-to-form"><span
                                                            class="pakat-large-icon"><</span> <?php esc_attr_e( 'Add to form', 'pakat_lang' ); ?>
                                                </button>&nbsp;&nbsp;
                                                <?php Pakat_Page_Home::get_narration_script( __( 'Add to form', 'pakat_lang' ), __( 'Please click where you want to insert the field and click on this button. By default, the new field will be added at top.', 'pakat_lang' ) ); ?>
                                            </div>
                                        </div>

                                        <!---- end block ------>
										<div id="pakat_form_captcha" class="panel panel-default row form-field"
											 style="padding-bottom: 20px;">
											<div class="alert alert-danger" style="margin:5px;display: none;"></div>
											<div class="row small-content2" style="margin-top: 15px;margin-bottom: 15px;">
												<b><?php esc_attr_e( 'Add Captcha', 'pakat_lang' ); ?></b>&nbsp;
												<?php Pakat_Page_Home::get_narration_script( __( 'Add Captcha', 'pakat_lang' ), __( 'We are using Google reCaptcha for this form. To use Google reCaptcha on this form, you should input site key and secret key.' , 'pakat_lang' ) ); ?>
											</div>
											<div class="row small-content2" style="margin-top: 0px;">
												<label class="pakat-radio-label">
                                                    &nbsp;<?php esc_attr_e( 'Yes', 'pakat_lang' ); ?>
                                                    <input type="radio" name="pakat_add_captcha" class="pakat-add-captcha" value="1" <?php checked( $gCaptcha, '1' ); ?>>
                                                </label>
												<label class="pakat-radio-label">
                                                    &nbsp;<?php esc_attr_e( 'No', 'pakat_lang' ); ?>
                                                    <input type="radio" name="pakat_add_captcha" class="pakat-add-captcha" value="0" <?php checked( $gCaptcha, '0' ); ?>>
                                                </label>
											</div>
											<div class="row small-content2 pakat-captcha-key"
											<?php
											if ( '1' !== $gCaptcha ) {
												echo("style='display: none;'");}
											?>
											>
												<i><?php esc_attr_e( 'Site Key', 'pakat_lang' ); ?></i>&nbsp;
												<input type="text" class="col-md-12" id="pakat_captcha_site" name="pakat_captcha_site" value="<?php
												if ( isset( $formData['gCaptcha_site'] ) && ! empty( $formData['gCaptcha_site'] ) ) {
													echo esc_attr( $formData['gCaptcha_site'] );
												} else {
													echo '';
												}
												?>">
											</div>
											<div class="row small-content2 pakat-captcha-key"
											<?php
											if ( '1' !== $gCaptcha ) {
												echo("style='display: none;'");}
											?>
											>
												<i><?php esc_attr_e( 'Secret Key', 'pakat_lang' ); ?></i>&nbsp;
												<input type="text" class="col-md-12" id="pakat_captcha_secret" name="pakat_captcha_secret" value="<?php
												if ( isset( $formData['gCaptcha_secret'] ) && ! empty( $formData['gCaptcha_secret'] ) ) {
													echo esc_attr( $formData['gCaptcha_secret'] );
												} else {
													echo '';
												}
												?>">
											</div>
											<div class="row small-content2 pakat-captcha-key"
												<?php
												if ( '1' !== $gCaptcha ) {
													echo("style='display: none;'");}
												?>
											>
												<label class="pakat-radio-label">&nbsp;
                                                    <?php esc_attr_e( 'Google Captcha', 'pakat_lang');?>
                                                    <input type="radio" name="pakat_recaptcha_type" class="pakat-captcha-type" value="0" <?php checked( $invipakatleCaptcha, '0' ); ?>>
                                                </label>
												<label class="pakat-radio-label">
                                                    &nbsp;<?php esc_attr_e( 'Google Invipakatle Captcha', 'pakat_lang');?>
                                                    <input type="radio" name="pakat_recaptcha_type" class="pakat-captcha-type" value="1" <?php checked( $invipakatleCaptcha, '1' ); ?>>
                                                </label>
											</div>
											<div class="row small-content2 pakat-captcha-key"
											<?php
											if ( '1' !== $gCaptcha ) {
												echo("style='display: none;'");}
											?>
											>
												<button type="button" id="pakat_add_captcha_btn"
														class="btn btn-default pakat-add-to-form"><span
														class="pakat-large-icon"><</span> <?php esc_attr_e( 'Add to form', 'pakat_lang' ); ?>
												</button>&nbsp;&nbsp;
												<?php Pakat_Page_Home::get_narration_script( __( 'Add Captcha', 'pakat_lang' ), __( 'Please click where you want to insert the field and click on this button. By default, the new field will be added at top.', 'pakat_lang' ) ); ?>
											</div>
											</div>
										<div id="pakat_form_terms" class="panel panel-default row form-field"
											 style="padding-bottom: 20px;">
											<div class="alert alert-danger" style="margin:5px;display: none;"></div>
											<!-- for terms -->
											<div class="row small-content2" style="margin-top: 15px;margin-bottom: 15px;">
												<b><?php esc_attr_e( 'Add a Term acceptance checkbox', 'pakat_lang' ); ?></b>&nbsp;
												<?php Pakat_Page_Home::get_narration_script( __( 'Add a Term acceptance checkbox', 'pakat_lang' ), __( 'If the terms and condition checkbox is added to the form, the field will be mandatory for subscription.' , 'pakat_lang' ) ); ?>
											</div>
											<div class="row small-content2" style="margin-top: 0px;">
												<label class="pakat-radio-label">&nbsp;
                                                    <?php esc_attr_e( 'Yes', 'pakat_lang');?>
                                                    <input type="radio" name="pakat_add_terms" class="pakat-add-terms" value="1" <?php checked( $formData['termAccept'], '1' ); ?>>
                                                </label>
												<label class="pakat-radio-label">&nbsp;
                                                    <?php esc_attr_e( 'No', 'pakat_lang');?>
                                                    <input type="radio" name="pakat_add_terms" class="pakat-add-terms" value="0" <?php checked( $formData['termAccept'], '0' ); ?>>
                                                </label>
											</div>
											<div class="row small-content2 pakat-terms-url"
											<?php
											if ( '1' !== $formData['termAccept'] ) {
												echo("style='display: none;'");}
											?>
											>
												<i><?php esc_attr_e( 'URL to terms and conditions', 'pakat_lang' ); ?></i>&nbsp;
												<input type="text" class="col-md-12" id="pakat_terms_url" name="pakat_terms_url" value="<?php
												if ( isset( $formData['termsURL'] ) && ! empty( $formData['termsURL'] ) ) {
													echo esc_attr( $formData['termsURL'] );
												} else {
													echo '';
												}
												?>">
											</div>
											<div class="row small-content2 pakat-terms-url"
											<?php
											if ( '1' !== $formData['termAccept'] ) {
												echo("style='display: none;'");}
											?>
											>
												<button type="button" id="pakat_add_termsUrl_btn"
														class="btn btn-default pakat-add-to-form"><span
														class="pakat-large-icon"><</span> <?php esc_attr_e( 'Add to form', 'pakat_lang' ); ?>
												</button>&nbsp;&nbsp;
												<?php Pakat_Page_Home::get_narration_script( __( 'Add Terms URL', 'pakat_lang' ), __( 'Please click where you want to insert the field and click on this button. By default, the new field will be added at top.', 'pakat_lang' ) ); ?>
											</div>

										</div>
										<!-- use css of custom or theme -->
										<div class="panel panel-default row form-field">
											<div class="row small-content2" style="margin-top: 15px;margin-bottom: 10px;">
												<b><?php esc_attr_e( 'Form Style', 'pakat_lang' ); ?>&nbsp;</b>
												<?php Pakat_Page_Home::get_narration_script( __( 'Form Style', 'pakat_lang' ), __( 'Select the style you favorite. Your custom css will be applied to form only.', 'pakat_lang' ) ); ?>
											</div>
											<div id="pakat_form_css_area" class="row small-content2" style="margin-bottom: 15px;">
												<label style="font-weight: normal;"><input type="radio" name="pakat_css_type" value="1" <?php checked( $formData['dependTheme'], '1' ); ?>>&nbsp;<?php esc_attr_e( 'Current Theme', 'pakat_lang' ); ?>
												</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<label style="font-weight: normal;"><input type="radio" name="pakat_css_type" value="0" <?php checked( $formData['dependTheme'], '0' ); ?>>&nbsp;<?php esc_attr_e( 'Custom style', 'pakat_lang' ); ?>
												</label>
												<textarea class="widefat" cols="60" rows="10" id="pakatcssmarkup" style="width: 100%; margin-top: 10px; font-size: 13px; display: <?php echo '0' == $formData['dependTheme'] ? 'block' : 'none'; ?>;"
														  name="pakat_form_css"><?php echo esc_textarea( $formData['css'] ); ?></textarea>

											</div>

										</div>
									</div>
									<div class="col-md-6">
										<!-- hidden fields for attributes -->
										<input type="hidden" id="pakat_hidden_email" data-type="email" data-name="email"
											   data-text="<?php esc_attr_e( 'Email Address', 'pakat_lang' ); ?>">
										<input type="hidden" id="pakat_hidden_submit" data-type="submit"
											   data-name="submit" data-text="<?php esc_attr_e( 'Subscribe', 'pakat_lang' ); ?>">
										<input type="hidden" id="pakat_hidden_message_1"
											   value="<?php esc_attr_e( 'Select Pakat Attribute', 'pakat_lang' ); ?>">
										<input type="hidden" id="pakat_hidden_message_2"
											   value="<?php esc_attr_e( 'Pakat merge fields : Normal', 'pakat_lang' ); ?>">
										<input type="hidden" id="pakat_hidden_message_3"
											   value="<?php esc_attr_e( 'Pakat merge fields : Category', 'pakat_lang' ); ?>">
										<input type="hidden" id="pakat_hidden_message_4"
											   value="<?php esc_attr_e( 'Other', 'pakat_lang' ); ?>">
										<input type="hidden" id="pakat_hidden_message_5"
											   value="<?php esc_attr_e( 'Submit Button', 'pakat_lang' ); ?>">

										<!-- preview field -->

										<div class="panel panel-default row form-field">
											<div class="row small-content2" style="margin-top: 15px;margin-bottom: 15px;">
												<b><?php esc_attr_e( 'Preview', 'pakat_lang' ); ?>&nbsp;
													<span id="pakat-preview-form-refresh"
														  class="glyphicon glyphicon-refresh"
														  style="cursor:pointer"></span></b>
											</div>
											<iframe id="pakat-preview-form"
													src="<?php echo esc_url( site_url() . '/?pakat_form=' . esc_attr( $this->formID ) ); ?>"
													width="300px" height="428"></iframe>
										</div>
									</div>
								</div>
								<div class="row pakat-small-content" style="margin-top: 30px;">
									<div class="col-md-3">
										<button class="btn btn-primary"><?php esc_attr_e( 'Save', 'pakat_lang' ); ?></button>
									</div>
								</div>
							</div>
						</div> <!-- End Subscription form-->

						<!-- Sign up Process -->

						<div class="panel panel-default row pakat-small-content">

							<!-- Adding security through hidden referrer field -->
							<div class="page-header">
								<strong><?php esc_attr_e( 'Sign up process', 'pakat_lang' ); ?></strong>&nbsp;<i
									id="pakat_setting_signup_spin" class="fa fa-spinner fa-spin fa-fw fa-lg fa-2x"></i>
							</div>
							<div id="pakat_setting_signup_body" class="panel-body">
								<div id="pakat_form_alert_message" class="alert alert-danger alert-dismissable fade in"
									 role="alert" style="display: none;">
									<span id="pakat_disclaim_smtp"
										  style="display: none;"><?php _e( 'Confirmation emails will be sent through your own email server, but you have no guarantees on their deliverability. <br/> <a href="https://app-smtp.pakat.net/" target="_blank">Click here</a> to send your emails through Pakat in order to improve your deliverability and get statistics', 'pakat_lang' ); ?></span>
									<span id="pakat_disclaim_do_template"
										  style="display: none;"><?php _e( 'The template you selected does not include a link [DOUBLEOPTIN] to allow subscribers to confirm their subscription. <br/> Please edit the template to include a link with [DOUBLEOPTIN] as URL.', 'pakat_lang' ); ?></span>
                                    <span id="pakat_disclaim_confirm_template"
                                          style="display: none;"><?php _e( 'You cannot select a template with the tag [DOUBLEOPTIN]', 'pakat_lang' ); ?></span>
								</div>

								<!-- Linked List -->
								<div class="row pakat-small-content">
									<span class="col-md-3">
										<?php esc_attr_e( 'Linked List', 'pakat_lang' ); ?>&nbsp;
										<?php Pakat_Page_Home::get_narration_script( __( 'Linked List', 'pakat_lang' ), __( 'Select the list where you want to add your new subscribers', 'pakat_lang' ) ); ?>
									</span>
									<div id="pakat_select_list_area" class="col-md-4">

										<input type="hidden" id="pakat_selected_list_id" value="">
                                        <select data-placeholder="Please select the list" id="pakat_select_list"
                                                class="col-md-12 chosen-select" name="list_id[]" multiple=""
                                                tabindex="-1"></select>
									</div>
									<div class="col-md-5">
										<small
											style="font-style: italic;"><?php esc_attr_e( 'You can use Marketing Automation to create specific workflow when a user is added to the list.', 'pakat_lang' ); ?></small>
									</div>

								</div>
								<!-- confirmation email -->
								<div class="row small-content">
									<span class="col-md-3"><?php esc_attr_e( 'Send a confirmation email', 'pakat_lang' ); ?><?php echo esc_html( Pakat_Page_Home::get_narration_script( __( 'Confirmation message', 'pakat_lang' ), __( 'You can choose to send a confirmation email. You will be able to set up the template that will be sent to your new suscribers', 'pakat_lang' ) ) ); ?></span>

									<div class="col-md-4">
										<label class="col-md-6" style="font-weight: normal;"><input type="radio"
																									id="is_confirm_email_yes"
																									name="is_confirm_email"
																									value="1" <?php checked( $formData['isOpt'], '1' ); ?>>&nbsp;<?php esc_attr_e( 'Yes', 'pakat_lang' ); ?>
										</label>
										<label class="col-md-6" style="font-weight: normal;"><input type="radio"
																									id="is_confirm_email_no"
																									name="is_confirm_email"
																									value="0" <?php checked( $formData['isOpt'], '0' ); ?>>&nbsp;<?php esc_attr_e( 'No', 'pakat_lang' ); ?>
										</label>
									</div>
									<div class="col-md-5">
										<small
											style="font-style: italic;"><?php esc_attr_e( 'Select "Yes" if you want your subscribers to receive a confirmation email', 'pakat_lang' ); ?></small>
									</div>
								</div>
								<!-- select template id for confirmation email -->
								<div class="row" id="pakat_confirm_template_area">
									<input type="hidden" id="pakat_selected_template_id"
										   value="<?php echo esc_attr( $formData['templateID'] ); ?>">
									<input type="hidden" id="pakat_default_template_name"
										   value="<?php esc_attr_e( 'Default', 'pakat_lang' ); ?>">

									<div class="col-md-3" id="pakat_template_id_area">
									</div>
									<div class="col-md-4">
										<a href="https://my.pakat.net/camp/lists/template" class="col-md-12"
										   target="_blank"><i
												class="fa fa-angle-right"></i> <?php esc_attr_e( 'Set up my templates', 'pakat_lang' ); ?>
										</a>
									</div>
								</div>
								<!-- double optin confirmation email -->
								<div class="row pakat-small-content">
									<span
										class="col-md-3"><?php esc_attr_e( 'Double Opt-In', 'pakat_lang' ); ?><?php echo esc_html( Pakat_Page_Home::get_narration_script( __( 'Double Opt-In', 'pakat_lang' ), __( 'Your subscribers will receive an email inviting them to confirm their subscription. Be careful, your subscribers are not saved in your list before confirming their subscription.', 'pakat_lang' ) ) ); ?></span>

									<div class="col-md-4">
										<label class="col-md-6" style="font-weight: normal;"><input type="radio"
																									id="is_double_optin_yes"
																									name="is_double_optin"
																									value="1" <?php checked( $formData['isDopt'], '1' ); ?>>&nbsp;<?php esc_attr_e( 'Yes', 'pakat_lang' ); ?>
										</label>
										<label class="col-md-6" style="font-weight: normal;"><input type="radio"
																									id="is_double_optin_no"
																									name="is_double_optin"
																									value="0" <?php checked( $formData['isDopt'], '0' ); ?>>&nbsp;<?php esc_attr_e( 'No', 'pakat_lang' ); ?>
										</label>
									</div>
									<div class="col-md-5">
										<small
											style="font-style: italic;"><?php esc_attr_e( 'Select "Yes" if you want your subscribers to confirm their email address', 'pakat_lang' ); ?></small>
									</div>
								</div>
								<!-- select template id for double optin confirmation email -->
								<div class="row" id="pakat_doubleoptin_template_area">
									<input type="hidden" id="pakat_selected_do_template_id" value="<?php echo esc_attr( $formData['templateID'] ); ?>">
									<div class="col-md-3" id="pakat_doubleoptin_template_id_area">
									</div>
									<div class="col-md-4">
										<a href="https://my.pakat.net/camp/lists/template"
										   class="col-md-12" target="_blank"><i
												class="fa fa-angle-right"></i> <?php esc_attr_e( 'Set up my templates', 'pakat_lang' ); ?>
										</a>
									</div>
								</div>
								<div class="row pakat-small-content" id="pakat_double_redirect_area">
									<span class="col-md-3"><?php esc_attr_e( 'Redirect to this URL after clicking in the email', 'pakat_lang' ); ?></span>

									<div class="col-md-8">
										<input type="url" class="col-md-11" name="redirect_url" value="<?php echo esc_attr( $formData['redirectInEmail'] ); ?>">
									</div>
								</div>
                                <div class="row pakat-small-content" id="pakat_final_confirm_template_area">
									<span class="col-md-3"><?php esc_attr_e( 'Select final confirmation email template', 'pakat_lang' ); ?><?php echo esc_html( Pakat_Page_Home::get_narration_script( __( 'Final confirmation', 'pakat_lang' ), __( 'This is the final confirmation email your contacts will receive once they click on the double opt-in confirmation link. You can select one of the default templates we have created for you, e.g. \'Default template - Final confirmation\'.
For your information, you cannot select a template with the tag [DOUBLEOPTIN].', 'pakat_lang' ) ) ); ?></span>
                                    <div class="col-md-8">
                                        <input type="hidden" id="pakat_selected_confirm_template_id" value="<?php echo esc_attr( $formData['confirmID'] );?>">
                                        <div class="col-md-5" id="pakat_final_confirm_template_id_area">
                                        </div>
                                        <div class="col-md-4">
                                            <a href="https://my.pakat.net/camp/lists/template"
                                               class="col-md-12" target="_blank"><i
                                                        class="fa fa-angle-right"></i> <?php esc_attr_e( 'Set up my templates', 'pakat_lang' ); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>

								<div class="row pakat-small-content">
									<span
										class="col-md-3"><?php esc_attr_e( 'Redirect to this URL after subscription', 'pakat_lang' ); ?></span>

									<div class="col-md-4">
										<label class="col-md-6" style="font-weight: normal;"><input type="radio"
																									id="is_redirect_url_click_yes"
																									name="is_redirect_url_click"
																									value="1" checked>&nbsp;<?php esc_attr_e( 'Yes', 'pakat_lang' ); ?>
										</label>
										<label class="col-md-6" style="font-weight: normal;"><input type="radio"
																									id="is_redirect_url_click_no"
																									name="is_redirect_url_click"
																									value="0" <?php checked( $formData['redirectInForm'], '' ); ?>>&nbsp;<?php esc_attr_e( 'No', 'pakat_lang' ); ?>
										</label>

									</div>
									<div class="col-md-5">
										<small
											style="font-style: italic;"><?php esc_attr_e( 'Select "Yes" if you want to redirect your subscribers to a specific page after they fullfill the form', 'pakat_lang' ); ?></small>
									</div>
								</div>
								<div class="row" style="margin-top: 10px;
								<?php
								if ( '' == $formData['redirectInForm'] ) {
									echo 'display:none;';
								}
								?>
								" id="pakat_subscrition_redirect_area">
									<span class="col-md-3"></span>

									<div class="col-md-8">
										<input type="url" class="col-md-11" name="redirect_url_click"
											   value="<?php echo esc_attr( $formData['redirectInForm'] ); ?>">
									</div>
								</div>

								<div class="row pakat-small-content" style="margin-top: 30px;">
									<div class="col-md-3">
										<button class="btn btn-primary"><?php esc_attr_e( 'Save', 'pakat_lang' ); ?></button>
									</div>
								</div>

							</div>
						</div><!-- End Sign up process form-->

						<!-- Confirmation message form -->
						<div class="panel panel-default row pakat-small-content">
							<div class="page-header">
								<strong><?php esc_attr_e( 'Confirmation message', 'pakat_lang' ); ?></strong>
							</div>
							<div class="panel-body">
								<div class="row pakat-small-content">
									<span class="col-md-3"><?php esc_attr_e( 'Success message', 'pakat_lang' ); ?></span>

									<div class="col-md-8">
										<input type="text" class="col-md-11" name="alert_success_message"
											   value="<?php echo esc_attr( $formData['successMsg'] ); ?>" required>&nbsp;
										<?php echo esc_html( Pakat_Page_Home::get_narration_script( __( 'Success message', 'pakat_lang' ), __( 'Set up the success message that will appear when one of your visitors surccessfully signs up', 'pakat_lang' ) ) ); ?>
									</div>
								</div>
								<div class="row pakat-small-content">
									<span class="col-md-3"><?php esc_attr_e( 'General error message', 'pakat_lang' ); ?></span>

									<div class="col-md-8">
										<input type="text" class="col-md-11" name="alert_error_message"
											   value="<?php echo esc_attr( $formData['errorMsg'] ); ?>" required>&nbsp;
										<?php echo esc_html( Pakat_Page_Home::get_narration_script( __( 'General message error', 'pakat_lang' ), __( 'Set up the message that will appear when an error occurs during the subscritpion process', 'pakat_lang' ) ) ); ?>
									</div>
								</div>
								<!--
								<div class="row pakat-small-content">
									<span class="col-md-3"><?php esc_attr_e( 'Existing subscribers', 'pakat_lang' ); ?></span>

									<div class="col-md-8">
										<input type="text" class="col-md-11" name="alert_exist_subscriber"
											   value="<?php echo esc_attr( $formData['existMsg'] ); ?>" required>&nbsp;
										<?php echo esc_html( Pakat_Page_Home::get_narration_script( __( 'Existing Suscribers', 'pakat_lang' ), __( 'Set up the message that will appear when a suscriber is already in your database', 'pakat_lang' ) ) ); ?>
									</div>
								</div>
								-->
								<div class="row pakat-small-content">
									<span class="col-md-3"><?php esc_attr_e( 'Invalid email address', 'pakat_lang' ); ?></span>

									<div class="col-md-8">
										<input type="text" class="col-md-11" name="alert_invalid_email"
											   value="<?php echo esc_attr( $formData['invalidMsg'] ); ?>" required>&nbsp;
										<?php echo esc_html( Pakat_Page_Home::get_narration_script( __( 'Invalid email address', 'pakat_lang' ), __( 'Set up the message that will appear when the email address used to sign up is not valid', 'pakat_lang' ) ) ); ?>
									</div>
								</div>
                                <div class="row pakat-small-content">
                                    <span class="col-md-3"><?php esc_attr_e( 'Required Field', 'pakat_lang' ); ?></span>

                                    <div class="col-md-8">
                                        <input type="text" class="col-md-11" name="alert_required_message"
                                               value="<?php echo esc_attr( $formData['requiredMsg'] ); ?>" required>&nbsp;
                                        <?php echo esc_html( Pakat_Page_Home::get_narration_script( __( 'Required Field', 'pakat_lang' ), __( 'Set up the message that will appear when the required field is empty', 'pakat_lang' ) ) ); ?>
                                    </div>
                                </div>
								<div class="row pakat-small-content" style="margin-top: 30px;">
									<div class="col-md-3">
										<button class="btn btn-primary"><?php esc_attr_e( 'Save', 'pakat_lang' ); ?></button>
									</div>
								</div>
							</div>
						</div> <!-- End Confirmation message form-->
					</form>
				</div>
				<script>
					jQuery(document).ready(function () {
						jQuery('#pakat_add_to_form_btn').click(function () {
							//var field_html = jQuery('#pakat_field_html').html();

							// tinyMCE.activeEditor.selection.setContent(field_html);

							return false;
						});
					});
				</script>
				<?php
			} else {
				// If empty?
				?>
				<div id="main-content" class="pakat-content">
					<div class="panel panel-default row pakat-small-content">
						<div class="page-header">
							<strong><?php esc_attr_e( 'Subscription form', 'pakat_lang' ); ?></strong>
						</div>
						<div style="padding: 24px 32px; margin-bottom: 12px;">
							<?php esc_attr_e( 'Sorry, you selected invalid form ID. Please check again if the ID is right', 'pakat_lang' ); ?>
						</div>
					</div>
				</div>
				<?php
			}
		}

		/** Generate welcome page */
		function generate_welcome_page() {
		?>
			<div id="main-content" class="row">
				<img class="small-content" src="<?php echo esc_url( Pakat_Manager::$plugin_url . '/img/background/setting.png' ); ?>" style="width: 100%;">
			</div>
		<?php
			Pakat_Page_Home::print_disable_popup();
		}

		/** Save subscription form setting */
		public static function save_setting_subscription() {
			// Check user role.
			if ( ! current_user_can( 'manage_options' ) ) {
				wp_die( 'Not allowed' );
			}

			// Check secret through hidden referrer field.
			check_admin_referer( 'pakat_setting_subscription' );

			//Handling of backslash added by WP because magic quotes are enabled by default
			array_walk_recursive( $_POST, function(&$value) {
				$value = stripslashes($value);
			});

			// Subscription form.
			$formID = isset( $_POST['pakat_form_id'] ) ? sanitize_text_field( $_POST['pakat_form_id'] ) : '';
			$form_name = isset( $_POST['pakat_form_name'] ) ? sanitize_text_field( $_POST['pakat_form_name'] ) : '';
			$form_html = isset( $_POST['pakat_form_html'] ) ? $_POST['pakat_form_html'] : '';
			$list_ids = isset( $_POST['list_id'] ) ? maybe_serialize( $_POST['list_id'] ) : '';
			$dependTheme = isset( $_POST['pakat_css_type'] ) ? sanitize_text_field( $_POST['pakat_css_type'] ) : '';
			$customCss = isset( $_POST['pakat_form_css'] ) ? $_POST['pakat_form_css'] : '';
			$gCaptcha = isset( $_POST['pakat_add_captcha'] ) ? sanitize_text_field( $_POST['pakat_add_captcha'] ) : '0';
			$gCaptchaSecret = isset( $_POST['pakat_captcha_secret'] ) ? sanitize_text_field( $_POST['pakat_captcha_secret'] ) : '';
			$gCaptchaSite = isset( $_POST['pakat_captcha_site'] ) ? sanitize_text_field( $_POST['pakat_captcha_site'] ) : '';
			$termAccept = isset( $_POST['pakat_add_terms'] ) ? sanitize_text_field( $_POST['pakat_add_terms'] ) : '0';
			$termURL = isset( $_POST['pakat_terms_url'] ) ? sanitize_text_field( $_POST['pakat_terms_url'] ) : '';
			$gCaptchaType = isset( $_POST['pakat_recaptcha_type'] ) ? sanitize_text_field( $_POST['pakat_recaptcha_type'] ) : '0';
			if ( $gCaptcha != '0' ) {
				if ( $gCaptchaType == '0' ) {
					$gCaptcha = '3';  // google recaptcha.
				}
				elseif ( $gCaptchaType == '1' ) {
					$gCaptcha = '2';   // google invipakatle recaptcha.
				}
			}
			// for wpml plugins.
			$pid = isset( $_POST['pid'] ) ? sanitize_text_field( $_POST['pid'] ) : '';
			$lang = isset( $_POST['lang'] ) ? sanitize_text_field( $_POST['lang'] ) : '';
			// sign up process.
			$templateID = '-1';
			$confirmID = '-1';
			$redirectInForm = '';

			$isOpt = isset( $_POST['is_confirm_email'] ) ? sanitize_text_field( $_POST['is_confirm_email'] ) : false;
			if ( $isOpt ) {
				$templateID = isset( $_POST['template_id'] ) ? sanitize_text_field( $_POST['template_id'] ) : '-1';
			}
			$isDopt = isset( $_POST['is_double_optin'] ) ? sanitize_text_field( $_POST['is_double_optin'] ) : false;
			if ( $isDopt ) {
				$templateID = isset( $_POST['doubleoptin_template_id'] ) ? sanitize_text_field( $_POST['doubleoptin_template_id'] ) : '-1';
                $confirmID = isset( $_POST['confirm_template_id'] ) ? sanitize_text_field( $_POST['confirm_template_id'] ) : '-1';
			}
			$redirectInEmail = isset( $_POST['redirect_url'] ) ? sanitize_text_field( $_POST['redirect_url'] ) : '';
			$isRedirectInForm = isset( $_POST['is_redirect_url_click'] ) ? sanitize_text_field( $_POST['is_redirect_url_click'] ) : false;
			if ( $isRedirectInForm ) {
				$redirectInForm = isset( $_POST['redirect_url_click'] ) ? sanitize_text_field( $_POST['redirect_url_click'] ) : '';
			}

			// get available attributes list.
			$attributes = Pakat_API_Manager::get_attributes();
			$attributes = array_merge( $attributes['attributes']['normal_attributes'],$attributes['attributes']['category_attributes'] );
			$available_attrs = array( 'email' );
			if ( isset( $attributes ) && is_array( $attributes ) ) {
				foreach ( $attributes as $attribute ) {
					$pos = strpos( $form_html, 'pakat-' . $attribute['name'] . '-area' );
					if ( false !== $pos ) {
						$available_attrs[] = $attribute['name'];
					}
				}
			}
			$successMsg = isset( $_POST['alert_success_message'] ) ? sanitize_text_field( esc_attr ($_POST['alert_success_message'] ) ) : '';
			$errorMsg = isset( $_POST['alert_error_message'] ) ? sanitize_text_field( esc_attr( $_POST['alert_error_message'] ) ) : '';
			$existMsg = isset( $_POST['alert_exist_subscriber'] ) ? sanitize_text_field( esc_attr( $_POST['alert_exist_subscriber'] ) ) : '';
			$invalidMsg = isset( $_POST['alert_invalid_email'] ) ? sanitize_text_field( esc_attr( $_POST['alert_invalid_email'] ) ) : '';
			$requiredMsg = isset( $_POST['alert_required_message']) ? sanitize_text_field( esc_attr($_POST['alert_required_message'])) : '';
			$formData = array(
				'title' => $form_name,
				'html' => $form_html,
				'css' => $customCss,
				'listID' => $list_ids,
				'dependTheme' => $dependTheme,
				'isOpt' => $isOpt,
				'isDopt' => $isDopt,
				'templateID' => $templateID,
				'confirmID' => $confirmID,
				'redirectInEmail' => $redirectInEmail,
				'redirectInForm' => $redirectInForm,
				'successMsg' => $successMsg,
				'errorMsg' => $errorMsg,
				'existMsg' => $existMsg,
				'invalidMsg' => $invalidMsg,
				'requiredMsg' => $requiredMsg,
				'attributes' => implode( ',', $available_attrs ),
				'gcaptcha'   => $gCaptcha,
				'gcaptcha_secret' => $gCaptchaSecret,
				'gcaptcha_site'   => $gCaptchaSite,
				'termAccept'      => $termAccept,
				'termsURL'        => $termURL,
			);
			if ( 'new' === $formID ) {
				$formID = Pakat_Forms::addForm( $formData );
				if ( '' !== $pid ) {
					$transID = Pakat_Forms_Lang::add_form_ID( $formID, $pid, $lang );
				}
			} else {
				Pakat_Forms::updateForm( $formID, $formData );
			}
			if ( '' !== $pid ) {
				wp_safe_redirect(
					add_query_arg(
						array(
							'page' => self::PAGE_ID,
							'action' => 'edit',
							'id' => $formID,
							'pid' => $pid,
							'lang' => $lang,
						), admin_url( 'admin.php' )
					)
				);
				exit();
			} else {
				wp_safe_redirect(
					add_query_arg(
						array(
							'page' => self::PAGE_ID,
							'action' => 'edit',
							'id' => $formID,
						), admin_url( 'admin.php' )
					)
				);
				exit();
			}
		}

		/**
		 * Get template lists of pakat
		 */
		public static function get_template_lists() {
			$pakat_mail = new PakatApiClient();
			$data = array(
				'templateStatus' => true
			);
			$response = $pakat_mail->getEmailTemplates( $data );
			if ( $pakat_mail->getLastResponseCode() === PakatApiClient::RESPONSE_CODE_OK) {
				return $response['templates'];
			}
			else {
				return null;
			}
		}


		/** Ajax process when change template id */
		public static function ajax_change_template() {
			check_ajax_referer( 'ajax_pakat_admin_nonce', 'security' );
			$template_id = isset( $_POST['template_id'] ) ? sanitize_text_field( $_POST['template_id'] ) : '';
			$pakat_mail = new PakatApiClient( );
			$data = array(
				'id' => $template_id,
			);
			$response = $pakat_mail->getEmailTemplate( $data["id"] );

			$ret_email = '-1';
			if ( $pakat_mail->getLastResponseCode() === PakatApiClient::RESPONSE_CODE_OK) {
				$from_email = $response[0]['sender']['email'];
				if ( '[DEFAULT_FROM_EMAIL]' == $from_email ) {
					$ret_email = '-1';
				} else {
					$ret_email = $from_email;
				}
			}
			wp_send_json( $ret_email );
		}

		/**
		 * Ajax module to get all lists.
		 */
		public static function ajax_get_lists() {
			check_ajax_referer( 'ajax_pakat_admin_nonce', 'security' );
			$lists = Pakat_API_Manager::get_lists();
			$frmID = isset( $_POST['frmid'] ) ? sanitize_text_field( $_POST['frmid'] ) : '';
			$formData = Pakat_Forms::getForm( $frmID );
			$result = array(
				'lists' => $lists,
				'selected' => $formData['listID'],
			);
			wp_send_json( $result );
		}

		/**
		 * Ajax module to get all templates.
		 */
		public static function ajax_get_templates() {
			check_ajax_referer( 'ajax_pakat_admin_nonce', 'security' );
			$templates = Pakat_API_Manager::get_templates();
			$result = array(
				'templates' => $templates,
			);
			wp_send_json( $result );
		}

		/**
		 * Ajax module to get all attributes.
		 */
		public static function ajax_get_attributes() {
			check_ajax_referer( 'ajax_pakat_admin_nonce', 'security' );
			$attrs = Pakat_API_Manager::get_attributes();
			$result = array(
				'attrs' => $attrs,
			);
			wp_send_json( $result );
		}

		/**
		 * Ajax module to update form html for preview
		 */
		public static function ajax_update_html() {
			check_ajax_referer( 'ajax_pakat_admin_nonce', 'security' );
			$gCaptchaType = isset( $_POST['gCaptchaType']) ? sanitize_text_field($_POST['gCaptchaType']) : '1';
			$gCaptcha = isset( $_POST['gCaptcha'] ) ? sanitize_text_field($_POST['gCaptcha']) : '0';
			if ( $gCaptcha != '0' ) {
				if( $gCaptchaType == '1' ) {
					$gCaptcha = '2';
				}
				elseif ( $gCaptchaType == '0' ) {
					$gCaptcha = '3';
				}
			}
			$formData = array(
				'html' => isset( $_POST['frmData'] ) ? $_POST['frmData'] : '',
				'css' => isset( $_POST['frmCss'] ) ? esc_attr($_POST['frmCss']) : '',
				'dependTheme' => isset( $_POST['isDepend'] ) ? sanitize_text_field($_POST['isDepend']) : '',
				'gCaptcha' => $gCaptcha,
				'gCaptcha_site' => isset( $_POST['gCaptchaSite'] ) ? sanitize_text_field($_POST['gCaptchaSite']) : ''
			);

			update_option( Pakat_Manager::PREVIEW_OPTION_NAME, $formData );
			die;
		}

		/**
		 * Ajax module to copy content from origin form for translation
		 */
		public static function ajax_copy_origin_form() {
			check_ajax_referer( 'ajax_pakat_admin_nonce', 'security' );
			$pID = isset( $_POST['pid'] ) ? sanitize_text_field( $_POST['pid'] ) : 1;
			$formData = Pakat_Forms::getForm( $pID );
			$html = $formData['html'];

			wp_send_json( $html );
		}
	}
}
