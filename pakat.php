<?php
/**
 * Plugin Name: Newsletter, SMTP, Email marketing and Subscribe forms by Pakat
 * Plugin URI: https://www.pakat.net/?r=wporg
 * Description: Manage your contact lists, subscription forms and all email and marketing-related topics from your wp panel, within one single plugin
 * Version: 3.1.13
 * Author: Pakat
 * Author URI: https://www.pakat.net/?r=wporg
 * License: GPLv2 or later
 *
 * @package Pakat
 */

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/**
 * Application entry point. Contains plugin startup class that loads on <i> pakat_init </i> action.
 */
if ( ! class_exists( 'Mailin' ) ) {
	require_once( 'inc/mailin.php' );
}
if ( ! class_exists( 'PakatApiClient' ) ) {
    require_once( 'inc/PakatApiClient.php' );
}
if ( ! class_exists( 'PakatAccount' ) ) {
    require_once( 'inc/PakatAccount.php' );
}
// For marketing automation.
if ( ! class_exists( 'Pakat' ) ) {
	require_once( 'inc/pakat.php' );
}

if ( ! class_exists( 'Pakat_Manager' ) ) {
	register_deactivation_hook( __FILE__, array( 'Pakat_Manager', 'deactivate' ) );
	register_activation_hook( __FILE__, array( 'Pakat_Manager', 'install' ) );
	register_uninstall_hook( __FILE__, array( 'Pakat_Manager', 'uninstall' ) );

	require_once( 'page/page-home.php' );
	require_once( 'page/page-form.php' );
	require_once( 'page/page-statistics.php' );
	require_once( 'page/page-scenarios.php' );
	require_once( 'widget/widget_form.php' );
	require_once( 'inc/table-forms.php' );
	require_once( 'inc/pakat-api-manager.php' );
	require_once( 'inc/pakat-sms-code.php' );
	require_once( 'model/model-forms.php' );
	require_once( 'model/model-users.php' );
	require_once( 'model/model-lang.php' );

	/**
	 * Class Pakat_Manager
	 */
	class Pakat_Manager {

		/** Main setting option name */
		const MAIN_OPTION_NAME = 'pakat_main_option';

		/** Home setting option name */
		const HOME_OPTION_NAME = 'pakat_home_option';

		/** Access token option name */
		const ACCESS_TOKEN_OPTION_NAME = 'pakat_token_store';

		/** Plugin language notice option name */
		const LANGUAGE_OPTION_NAME = 'pakat_language_notice_option';

		/** Temp list of Dopt option name */
		const TEMPLIST_OPTION_NAME = 'pakat_temp_list';

		/** Form preview option name */
		const PREVIEW_OPTION_NAME = 'pakat_preview_form';

        const API_KEY_V3_OPTION_NAME = 'pakat_api_key_v3';

		const RECAPTCHA_API_TEMPLATE = 'https://www.google.com/recaptcha/api/siteverify?%s';

		/** Installation id option name */
		const INSTALLATION_ID = 'pakat_installation_id';

        /**
		 * API key
		 *
		 * @var $access_key
		 */
		public static $access_key;

		/**
		 * Store instance
		 *
		 * @var $instance
		 */
		public static $instance;

		/**
		 * Plugin directory path value. set in constructor
		 *
		 * @var $plugin_dir
		 */
		public static $plugin_dir;

		/**
		 * Plugin url. set in constructor
		 *
		 * @var $plugin_url
		 */
		public static $plugin_url;

		/**
		 * Plugin name. set in constructor
		 *
		 * @var $plugin_name
		 */
		public static $plugin_name;

		/**
		 * Check if wp_mail is declared
		 *
		 * @var $wp_mail_conflict
		 */
		static $wp_mail_conflict;

		/**
		 * Class constructor
		 * Sets plugin url and directory and adds hooks to <i>init</i>. <i>admin_menu</i>
		 */
		function __construct() {
			// get basic info.
			self::$plugin_dir = plugin_dir_path( __FILE__ );
			self::$plugin_url = plugins_url( '', __FILE__ );
			self::$plugin_name = plugin_basename( __FILE__ );

			self::$wp_mail_conflict = false;

			// api key for pakat.
			$general_settings = get_option( self::MAIN_OPTION_NAME, array() );
			self::$access_key = isset( $general_settings['access_key'] ) ? $general_settings['access_key'] : '';

			self::$instance = $this;

			add_action( 'admin_init', array( &$this, 'admin_init' ), 9999 );
			add_action( 'admin_menu', array( &$this, 'admin_menu' ), 9999 );

			add_action( 'wp_print_scripts', array( &$this, 'frontend_register_scripts' ), 9999 );
			add_action( 'wp_enqueue_scripts', array( &$this, 'wp_head_ac' ), 999 );

			// create custom url for form preview.
			add_filter( 'query_vars', array( &$this, 'pakat_query_vars' ) );
			add_action( 'parse_request', array( &$this, 'pakat_parse_request' ) );

			add_action( 'wp_ajax_pakat_validate_process', array( 'Pakat_Page_Home', 'ajax_validation_process' ) );
			add_action( 'wp_ajax_pakat_validate_ma', array( 'Pakat_Page_Home', 'ajax_validate_ma' ) );
			add_action( 'wp_ajax_pakat_activate_email_change', array( 'Pakat_Page_Home', 'ajax_activate_email_change' ) );
			add_action( 'wp_ajax_pakat_sender_change', array( 'Pakat_Page_Home', 'ajax_sender_change' ) );
			add_action( 'wp_ajax_pakat_send_email', array( 'Pakat_Page_Home', 'ajax_send_email' ) );
			add_action( 'wp_ajax_pakat_remove_cache', array( 'Pakat_Page_Home', 'ajax_remove_cache' ) );
			add_action( 'wp_ajax_pakat_sync_users', array( 'Pakat_Page_Home', 'ajax_sync_users' ) );

			add_action( 'wp_ajax_pakat_change_template', array( 'Pakat_Page_Form', 'ajax_change_template' ) );
			add_action( 'wp_ajax_pakat_get_lists', array( 'Pakat_Page_Form', 'ajax_get_lists' ) );
			add_action( 'wp_ajax_pakat_get_templates', array( 'Pakat_Page_Form', 'ajax_get_templates' ) );
			add_action( 'wp_ajax_pakat_get_attributes', array( 'Pakat_Page_Form', 'ajax_get_attributes' ) );
			add_action( 'wp_ajax_pakat_update_form_html', array( 'Pakat_Page_Form', 'ajax_update_html' ) );
			add_action( 'wp_ajax_pakat_copy_origin_form', array( 'Pakat_Page_Form', 'ajax_copy_origin_form' ) );

			add_action( 'wp_ajax_pakat_get_country_prefix', array( $this, 'ajax_get_country_prefix' ) );
			add_action( 'wp_ajax_nopriv_pakat_get_country_prefix', array( $this, 'ajax_get_country_prefix' ) );

			add_action( 'init', array( &$this, 'init' ) );

			add_action( 'wp_login', array( &$this, 'pakat_wp_login_identify' ), 10, 2 );

			// change pakat tables name on prior(2.6.9) versions.
			Pakat_Model_Users::add_prefix();
			Pakat_Forms::add_prefix();
			Pakat_Forms::modify_datatype();

			if ( self::is_api_key_set() ) {
				add_shortcode( 'pakatwp_form', array( &$this, 'pakatwp_form_shortcode' ) );
				// register widget.
				add_action( 'widgets_init', array( &$this, 'pakat_create_widget' ) );

                // create forms tables and create default form.
                Pakat_Forms::createTable();
                // create users table.
                Pakat_Model_Users::createTable();
                // add columns for old versions
                Pakat_Forms::alterTable();
			}

			$use_api_version = get_option( 'pakat_use_apiv2', '0' );
			if ( '0' === $use_api_version ) {
				self::uninstall();
				update_option( 'pakat_use_apiv2', '1' );
			}

			// Wpml plugin part.
			if ( ! function_exists( 'is_plugin_active_for_network' ) ) :
				require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
			endif;
			if ( in_array( 'sitepress-multilingual-cms/sitepress.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) || is_plugin_active_for_network( 'sitepress-multilingual-cms/sitepress.php' ) ) {
				Pakat_Forms_Lang::createTable();
				add_action( 'pakat_language_sidebar', array( $this, 'pakat_create_language_sidebar' ) );
			}

			/**
			 * Hook wp_mail to send transactional emails
			 */

			// check if wp_mail function is already declared by others.
			if ( function_exists( 'wp_mail' ) ) {
				self::$wp_mail_conflict = true;
			}
			$home_settings = get_option( Pakat_Manager::HOME_OPTION_NAME, array() );

			if( 'yes' === $home_settings['activate_email'] )
            {
                if ( false === self::$wp_mail_conflict ) {
                    /**
                     * Declare wp_mail function for Pakat SMTP module
                     *
                     * @param string $to - receiption email.
                     * @param string $subject - subject of email.
                     * @param string $message - message content.
                     * @param string $headers - header of email.
                     * @param array  $attachments - attachments.
                     * @return bool
                     */
                    function wp_mail( $to, $subject, $message, $headers = '', $attachments = array() ) {
                        $message = str_replace( 'NF_Pakat', '', $message );
                        $message = str_replace( 'WC_Pakat', '', $message );
                        try {
                            $sent = Pakat_Manager::pakat_email( $to, $subject, $message, $headers, $attachments );
                            if ( is_wp_error( $sent ) || ! isset( $sent['code'] ) || 'success' !== $sent['code'] ) {
				try{
				    return true;
				}catch( Exception $e ){
				    return false;
				}
                            }
                            return true;
                        } catch ( Exception $e ) {
			    return false;
                        }
                    }
                } else {
                    add_action( 'admin_notices', array( &$this, 'wpMailNotices' ) );
                    return;
                }
            }
		}

		/**
		 * Add identify tag for login users
		 *
		 * @param string $user_login - user login name.
		 * @param array  $user - user.
		 */
		function pakat_wp_login_identify( $user_login, $user ) {

			$userEmail = $user->user_email;
			$data = array(
				'email_id' => $userEmail,
				'name' => $user_login,
			);
			Pakat_API_Manager::identify_user( $data );
		}

		/**
		 * Initialize method. called on <i>init</i> action
		 */
		function init() {
			// Sign up process.
			if ( isset( $_POST['pakat_form_action'] ) && ( 'subscribe_form_submit' == sanitize_text_field($_POST['pakat_form_action']) ) ) {
				$this->signup_process();
			}
			// Subscribe.
			if ( isset( $_GET['pakat_action'] ) && ( 'subscribe' == sanitize_text_field($_GET['pakat_action']) ) ) {
				Pakat_API_Manager::subscribe();
				exit;
			}
			// Dismiss language notice.
			if ( isset( $_GET['dismiss_admin_lang_notice'] ) && '1' == sanitize_text_field($_GET['dismiss_admin_lang_notice']) ) {
				update_option( Pakat_Manager::LANGUAGE_OPTION_NAME, true );
				wp_safe_redirect( $_SERVER['HTTP_REFERER'] );
				exit();
			}

			add_action( 'wp_head', array( &$this, 'install_ma_script' ) );
		}

		/**
		 * Hook admin_init
		 */
		function admin_init() {
			add_action( 'admin_action_pakat_setting_subscription', array( 'Pakat_Page_Form', 'save_setting_subscription' ) );
			add_action( 'admin_action_nopriv_pakat_setting_subscription', array( 'Pakat_Page_Form', 'save_setting_subscription' ) );
			Pakat_Manager::LoadTextDomain();
			$this->register_scripts();
			$this->register_styles();
		}

		/**
		 * Hook admin_menu
		 */
		function admin_menu() {
			Pakat_Manager::LoadTextDomain();
			new Pakat_Page_Home();
			new Pakat_Page_Form();
			new Pakat_Page_Statistics();
			$home_settings = get_option( Pakat_Manager::HOME_OPTION_NAME );
			if ( isset( $home_settings['activate_ma'] ) && 'yes' == $home_settings['activate_ma'] ) {
				new Pakat_Page_Scenarios();
			}

		}

		/**
		 * Register script for admin page
		 */
		function register_scripts() {
			wp_register_script( 'pakat-bootstrap-js', self::$plugin_url . '/js/bootstrap/js/bootstrap.min.js', array( 'jquery' ), null );
			wp_register_script( 'pakat-admin-js', self::$plugin_url . '/js/admin.js', array( 'jquery' ), filemtime( self::$plugin_dir . '/js/admin.js' ) );
			wp_register_script( 'pakat-chosen-js', self::$plugin_url . '/js/chosen.jquery.min.js', array( 'jquery' ), null );
            wp_enqueue_script('jquery-ui-datepicker');
            wp_enqueue_script('jquery-ui-spinner');
        }

		/**
		 * Register stylesheet for admin page
		 */
		function register_styles() {
			wp_register_style( 'pakat-bootstrap-css', self::$plugin_url . '/js/bootstrap/css/bootstrap.css', array(), null, 'all' );
			wp_register_style( 'pakat-fontawesome-css', self::$plugin_url . '/css/fontawesome/css/font-awesome.css', array(), null, 'all' );
			wp_register_style( 'pakat-chosen-css', self::$plugin_url . '/css/chosen.min.css' );
			wp_register_style( 'pakat-admin-css', self::$plugin_url . '/css/admin.css', array(), filemtime( self::$plugin_dir . '/css/admin.css' ), 'all' );
		}

		/**
		 * Registers scripts for frontend
		 */
		function frontend_register_scripts() {

		}

		/**
		 * Enqueue script on front page
		 */
		function wp_head_ac() {
			wp_enqueue_script( 'pakat-front-js', self::$plugin_url . '/js/pakat-front.js', array( 'jquery' ), filemtime( self::$plugin_dir . '/js/pakat-front.js' ), false );
			wp_enqueue_style( 'pakat-front-css', self::$plugin_url.'/css/pakat-front.css', array(), array(), 'all');
			wp_localize_script(
				'pakat-front-js', 'pakatErrMsg', array(
					'invalidMail' => __( 'Please fill out valid email address', 'pakat_lang' ),
					'requiredField' => __( 'Please fill out required fields', 'pakat_lang' ),
					'invalidDateFormat' => __( 'Please fill out valid date format', 'pakat_lang' ),
                    'invalidSMSFormat' => __( 'Please fill out valid phone number', 'pakat_lang' ),
				)
			);
            wp_localize_script(
                'pakat-front-js', 'ajax_pakat_front_object',
                array(
                    'ajax_url' => admin_url( 'admin-ajax.php' ),
                    'ajax_nonce' => wp_create_nonce( 'pakat_front_ajax_nonce' ),
                    'flag_url' => plugins_url('img/flags/', __FILE__ ),
                )
            );
		}

		/**
		 * Install method is called once install this plugin.
		 * create tables, default option ...
		 */
		static function install() {
			$general_settings = get_option( self::MAIN_OPTION_NAME, array() );
			$access_key = isset( $general_settings['access_key'] ) ? $general_settings['access_key'] : '';
			if ( '' === $access_key ) {
				// Default option when activate.
				$home_settings = array(
					'activate_email' => 'no',
					'activate_ma' => 'no',
				);
				update_option( self::HOME_OPTION_NAME, $home_settings );
			}
		}

		/**
		 * Uninstall method is called once uninstall this plugin
		 * delete tables, options that used in plugin
		 */
		static function uninstall() {
			$setting = array();
			update_option( Pakat_Manager::MAIN_OPTION_NAME, $setting );

			$home_settings = array(
				'activate_email' => 'no',
				'activate_ma' => 'no',
			);
			update_option( Pakat_Manager::HOME_OPTION_NAME, $home_settings );

			// Delete access_token.
			$token_settings = array();
			update_option( Pakat_Manager::ACCESS_TOKEN_OPTION_NAME, $token_settings );
            delete_option(Pakat_Manager::API_KEY_V3_OPTION_NAME);
			// Empty tables.
			Pakat_Model_Users::removeTable();
			Pakat_Forms::removeTable();
			Pakat_Forms_Lang::removeTable();

			// Remove all transient.
			Pakat_API_Manager::remove_transients();
		}

		/**
		 * Deactivate method is called once deactivate this plugin
		 */
		static function deactivate() {
			update_option( Pakat_Manager::LANGUAGE_OPTION_NAME, false );
			// Remove sync users option.
			delete_option( 'pakat_sync_users' );
			// Remove all transient.
			Pakat_API_Manager::remove_transients();
		}

    /**
     * Check if plugin is logged in.
     *
     * @param bool $redirect
     * @return bool
     */
		static function is_done_validation($redirect = true) {
            if (self::is_api_key_set()) {
                $apiClient = new PakatApiClient();
                $apiClient->getAccount();
                if ( PakatApiClient::RESPONSE_CODE_OK === $apiClient->getLastResponseCode() ) {
                    return true;
                } elseif (PakatApiClient::RESPONSE_CODE_UNAUTHORIZED === $apiClient->getLastResponseCode()) {
                    delete_option(Pakat_Manager::API_KEY_V3_OPTION_NAME);
                }
			}

            if ($redirect) {
                self::redirect_to_pakat_plugin_homepage();
            }

            return false;
		}

        static function redirect_to_pakat_plugin_homepage() {
            wp_safe_redirect(add_query_arg('page', Pakat_Page_Home::PAGE_ID, admin_url('admin.php')));
        }

        /**
         * @return bool
         */
		static function is_api_key_set() {
		    $api_key = get_option(Pakat_Manager::API_KEY_V3_OPTION_NAME);
		    return !empty($api_key);
        }

		/**
		 * Install marketing automation script in header
		 */
		function install_ma_script() {
			$home_settings = get_option( Pakat_Manager::HOME_OPTION_NAME, array() );
			if ( isset( $home_settings['activate_ma'] ) && 'yes' == $home_settings['activate_ma'] ) {
				$general_settings = get_option( Pakat_Manager::MAIN_OPTION_NAME, array() );
				$ma_email = '';
				$current_user = wp_get_current_user();
				if ( $current_user instanceof WP_User ) {
					$ma_email = $current_user->user_email;
				}
				$ma_key = $general_settings['ma_key'];
				$output = '<script type="text/javascript">
			(function() {window.sib ={equeue:[],client_key:"'. $ma_key .'"};/* OPTIONAL: email for identify request*/
			window.sib.email_id = "'. $ma_email .'";
			window.sib = {}; for (var j = [\'track\', \'identify\', \'trackLink\', \'page\'], i = 0; i < j.length; i++) { (function(k) { window.sib[k] = function() { var arg = Array.prototype.slice.call(arguments); (window.sib[k] || function() { var t = {}; t[k] = arg; window.sib.equeue.push(t);})(arg[0], arg[1], arg[2]);};})(j[i]);}var n = document.createElement("script"),i = document.getElementsByTagName("script")[0]; n.type = "text/javascript", n.id = "pakat-js", n.async = !0, n.src = "https://automation.my.pakat.net/sa.js?key=" + window.sib.client_key, i.parentNode.insertBefore(n, i), window.sib.page();})();
            </script>';
				echo $output;
			}
		}

		/**
		 * Register widget
		 */
		function pakat_create_widget() {
			register_widget( 'Pakat_Widget_Subscribe' );
		}

		/**
		 * Display form on front page
		 *
		 * @param string $frmID - form ID.
		 * @param string $lang - form language.
		 */
		function generate_form_box( $frmID = '-1', $lang = '' ) {
			if ( 'oldForm' == $frmID ) {
				$frmID = get_option( 'pakat_old_form_id' );
			} elseif ( '' != $lang ) {
				$trans_id = Pakat_Forms_Lang::get_form_ID( $frmID, $lang );
				if ( null != $trans_id ) {
					$frmID = $trans_id;
				}
			}

			$formData = Pakat_Forms::getForm( $frmID );

			if ( empty( $formData ) ) {
				return;
			}
			// Add Google recaptcha
			if( '0' != $formData['gCaptcha'] ) {
				if( '1' == $formData['gCaptcha'] ) {   // For old forms.
					$formData['html'] = preg_replace( '/([\s\S]*?)<div class="g-recaptcha"[\s\S]*?data-size="invipakatle"><\/div>/', '$1', $formData['html'] );
				}
				if ( '3' == $formData['gCaptcha'] )     // The case of using google recaptcha.
				{
					?>
					<script type="text/javascript" charset="utf-8">
						var gCaptchaPakatWidget;
						var onloadPakatCallback = function() {
							var recaptchas = document.querySelectorAll('div[id=pakat_captcha]');
							for( i = 0; i < recaptchas.length; i++) {
								gCaptchaPakatWidget = grecaptcha.render(recaptchas[i], {
								'sitekey' : '<?php echo $formData["gCaptcha_site"] ?>'
								});
							}
						}
					</script>
					<?php
				}
				else {                                  // The case of using google invipakatle recaptcha.
					?>
					<script type="text/javascript">
						var gCaptchaPakatWidget;
						var onloadPakatCallback = function() {
							var element = document.getElementsByClassName('pakat-default-btn');
							gCaptchaPakatWidget = grecaptcha.render(element[0],{
								'sitekey' : '<?php echo $formData["gCaptcha_site"] ?>',
								'callback' : pakatVerifyCallback
							});
						};
					</script>
					<?php
				}
				?>
				<script src="https://www.google.com/recaptcha/api.js?onload=onloadPakatCallback&render=explicit" async defer></script>
				<?php
			}

			?>
			<form id="pakat_signup_form_<?php echo esc_attr( $frmID ); ?>" method="post" class="pakat_signup_form">
				<div class="pakat_loader" style="display:none;"><img
							src="<?php echo esc_url( includes_url() ); ?>images/spinner.gif" alt="loader"></div>
				<input type="hidden" name="pakat_form_action" value="subscribe_form_submit">
				<input type="hidden" name="pakat_form_id" value="<?php echo esc_attr( $frmID ); ?>">
                <input type="hidden" name="pakat_form_alert_notice" value="<?php echo esc_attr($formData['requiredMsg']); ?>">
                <input type="hidden" name="pakat_security" value="<?php echo esc_attr( wp_create_nonce( 'pakat_front_ajax_nonce' ) ); ?>">
				<div class="pakat_signup_box_inside_<?php echo esc_attr( $frmID ); ?>">
					<div style="/*display:none*/" class="pakat_msg_disp">
					</div>
					<?php
					echo stripcslashes($formData['html']);
					?>
				</div>
			</form>
			<style>
				<?php

				if ( ! $formData['dependTheme'] ) {
					// Custom css.
					$formData['css'] = str_replace( '[form]', 'form#pakat_signup_form_' . $frmID, $formData['css'] );
					echo $formData['css'];
				}
					$msgCss = str_replace( '[form]', 'form#pakat_signup_form_' . $frmID, Pakat_Forms::getDefaultMessageCss() );
					echo $msgCss;
				?>
			</style>
			<?php
		}

		/**
		 * Shortcode for sign up form
		 *
		 * @param array $atts - shortcode parameter.
		 * @return string
		 */
		function pakatwp_form_shortcode( $atts ) {
			$pull_atts = shortcode_atts(
				array(
					'id' => 'oldForm', // We will return 'oldForm' for shortcode of old form.
				), $atts
			);
			$frmID = $pull_atts['id'];
			$lang = defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : '';

			ob_start();
			$this->generate_form_box( $frmID, $lang );

			$output_string = ob_get_contents();
			ob_end_clean();
			return $output_string;
		}

		/**
		 * Sign up process
		 */
		function signup_process() {
			//Handling of backslash added by WP because magic quotes are enabled by default
			array_walk_recursive( $_POST, function(&$value) {
				$value = stripslashes($value);
			});

			if ( empty( $_POST['pakat_security'] ) ) {
				wp_send_json(
					array(
						'status' => 'pakat_security',
						'msg' => 'Token not found.',
					)
				);
			}
			$formID = isset( $_POST['pakat_form_id'] ) ? sanitize_text_field( $_POST['pakat_form_id'] ) : 1;
			if ( 'oldForm' == $formID ) {
				$formID = get_option( 'pakat_old_form_id' );
			}
			$formData = Pakat_Forms::getForm( $formID );

            if (!Pakat_Manager::is_done_validation(false) || 0 == count($formData)) {
                wp_send_json(
                    array(
                        'status' => 'failure',
                        'msg' => array("errorMsg" => "Something wrong occurred"),
                    )
                );
            }

			if ( '0' != $formData['gCaptcha'] ) {
				if ( ! isset( $_POST['g-recaptcha-response'] ) || empty( $_POST['g-recaptcha-response'] ) ) {
					wp_send_json(
						array(
							'status' => 'gcaptchaEmpty',
							'msg' => 'Please click on the reCAPTCHA box.',
						)
					);
				}
				$secret = $formData['gCaptcha_secret'];

				$data = array(
					'secret' => $secret,
					'response' => sanitize_text_field( $_POST['g-recaptcha-response'] ),
				);

                $args = [
                    'method' => 'POST',
                ];

                try {
                    $data = wp_remote_retrieve_body(wp_remote_request(sprintf(self::RECAPTCHA_API_TEMPLATE,  http_build_query($data)), $args));
                    $responseData = json_decode($data);
                    if ( ! $responseData->success ) {
                        wp_send_json(
                            array(
                                'status' => 'gcaptchaFail',
                                'msg' => 'Robot verification failed, please try again.',
                            )
                        );
                    }
                } catch (Exception $exception) {
                    wp_send_json(
                        array(
                            'status' => 'gcaptchaFail',
                            'msg' => $exception->getMessage(),
                        )
                    );
                }
			}

			$listID = $formData['listID'];
			if (empty($listID)) {
				$listID = array();
			}
			$interestingLists = isset( $_POST['interestingLists']) ?  array_map( 'esc_attr', $_POST['interestingLists'] ) : array();
			$expectedLists = isset( $_POST['listIDs'] ) ? array_map( 'esc_attr', $_POST['listIDs'] ) : array();
			if ( empty($interestingLists) )
            {
                $unlinkedLists = [];
            }
            else{
			    $unwantedLists = array_diff( $interestingLists, $expectedLists );
			    $unlinkedLists = array_diff( $unwantedLists, $listID);
			    $listID = array_unique(array_merge( $listID, $expectedLists ));
            }

			$email = isset( $_POST['email'] ) ? sanitize_email( $_POST['email'] ) : '';
			if ( ! is_email( $email ) ) {
				return;
			}

			$isDoubleOptin = $formData['isDopt'];
			$isOptin = $formData['isOpt'];
			$redirectUrlInEmail = $formData['redirectInEmail'];
			$redirectUrlInForm = $formData['redirectInForm'];

			$info = array();
			$attributes = explode( ',', $formData['attributes'] ); // String to array.
            if ( isset( $attributes ) && is_array( $attributes ) ) {
                foreach ( $_POST as $postAttribute => $postAttributeValue ) {
                    $correspondingPakatAttribute = $this->getCorrespondingPakatAttribute($postAttribute, $attributes);
                    if (!empty($correspondingPakatAttribute)) {
                        $info[ $correspondingPakatAttribute ] = sanitize_text_field( $postAttributeValue );
                    }
                }
            }
			$templateID = $formData['templateID'];

			if ( $isDoubleOptin ) {
				/*
				 * Double optin process
                 * 1. add record to db
                 * 2. send confirmation email with activate code
                 */
				$result = "success";
				// Send a double optin confirm email.
				if ( 'success' == $result ) {
					// Add a recode with activate code in db.
					$activateCode = $this->create_activate_code( $email, $info, $formID, $listID, $redirectUrlInEmail, $unlinkedLists );
					Pakat_API_Manager::send_comfirm_email( 'double-optin', $email, $templateID, $info, $activateCode );
				}
			} elseif ( $isOptin ) {
				$result = Pakat_API_Manager::create_subscriber( 'confirm', $email, $listID, $info, $unlinkedLists );
				if ( 'success' == $result ) {
					// Send a confirm email.
					Pakat_API_Manager::send_comfirm_email( 'confirm', $email, $templateID, $info );
				}
			} else {
				$result = Pakat_API_Manager::create_subscriber( 'simple', $email, $listID, $info, $unlinkedLists );
			}
			$msg = array(
				'successMsg' => $formData['successMsg'],
				'errorMsg' => $formData['errorMsg'],
				'existMsg' => $formData['existMsg'],
				'invalidMsg' => $formData['invalidMsg'],
			);

			wp_send_json(
				array(
					'status' => $result,
					'msg' => $msg,
					'redirect' => $redirectUrlInForm,
				)
			);
		}

		/**
		 * Create activate code for Double optin
		 *
		 * @param string $email  - user email.
		 * @param array  $info  - info.
		 * @param string $formID - form ID.
		 * @param array  $listIDs - lists.
		 * @param string $redirectUrl - redirect url.
		 * @return string - activate code.
		 */
		function create_activate_code( $email, $info, $formID, $listIDs, $redirectUrl, $unlinkedLists = null ) {
			$data = Pakat_Model_Users::get_data_by_email( $email, $formID );
			if ( $unlinkedLists != null )
            {
                $info['unlinkedLists'] = $unlinkedLists;
            }
			if ( false == $data ) {
				$uniqid = uniqid();
				$data = array(
					'email' => $email,
					'code' => $uniqid,
					'info' => maybe_serialize( $info ),
					'frmid' => $formID,
					'listIDs' => maybe_serialize( $listIDs ),
					'redirectUrl' => $redirectUrl,
				);
				Pakat_Model_Users::add_record( $data );
			} else {
				$uniqid = $data['code'];
			}
			return $uniqid;
		}

		/**
		 * Use Pakat SMTP to send all emails
		 *
		 * @param string $to - reception email.
		 * @param string $subject - subject of email.
		 * @param string $message - message of email.
		 * @param string $headers - header of email.
		 * @param array  $attachments - attachments.
		 */
		static function wp_mail_native( $to, $subject, $message, $headers = '', $attachments = array() ) {
			$result = require self::$plugin_dir . '/inc/function.wp_mail.php';
			return $result;
		}

		/**
		 * To send the transactional email via Pakat
		 * hook wp_mail
		 *
		 * @param string $to - reception email.
		 * @param string $subject - subject of email.
		 * @param string $message - message of email.
		 * @param string $headers - header of email.
		 * @param array  $attachments - attachments
		 * @param array  $tags - tag.
		 * @param string $from_name - sender name.
		 * @param string $from_email - sender email.
		 * @return mixed|WP_Error
		 */
		static function pakat_email( $to, $subject, $message, $headers = '', $attachments = array(), $tags = array(), $from_name = '', $from_email = '' ) {
            $data = [];
		    // Compact the input, apply the filters, and extract them back out.
			extract( apply_filters( 'wp_mail', compact( 'to', 'subject', 'message', 'headers', 'attachments' ) ) );

			if ( !empty( $attachments ) && ! is_array( $attachments ) ) {
				$attachments = explode( "\n", str_replace( "\r\n", "\n", $attachments ) );
			}

			// From email and name.
			$home_settings = get_option( Pakat_Manager::HOME_OPTION_NAME );
			if ( isset( $home_settings['sender'] ) ) {
				$from_name = $home_settings['from_name'];
				$from_email = $home_settings['from_email'];
			} else {
				$from_email = trim( get_bloginfo( 'admin_email' ) );
				$from_name = trim( get_bloginfo( 'name' ) );
			}
			$from_email = apply_filters( 'wp_mail_from', $from_email );
			$from_name = apply_filters( 'wp_mail_from_name', $from_name );

			if ( !empty( $headers ) ) {
			    if( is_array( $headers ) ){
                    		foreach ($headers as $key => $val) {
                        	    if( stripos($val, "Content-Type: text/html") !== false ) {
                            		unset( $headers[$key] );
                        	    }
                    		}
                    		$headers = array_values( $headers );
                    		if( count( $headers ) == 1 && $headers[0] == '' ) {
                        	    unset( $headers[0] );
                    		}
                	    }
			    if( is_string( $headers ) ){
				$headers = str_replace("Content-Type: text/html", "", $headers);
			    }
			    if( !empty( $headers ) ){
				$data['headers'] = $headers;
			    }
				if ( ! is_array( $headers ) ) {
					// Explode the headers out, so this function can take both.
					// string headers and an array of headers.
					$tempheaders = explode( "\n", str_replace( "\r\n", "\n", $headers ) );
				} else {
					$tempheaders = $headers;
				}
				$headers = array();
				$bcc = array();
				// If it's actually got contents.
				if ( ! empty( $tempheaders ) ) {
					// Iterate through the raw headers.
					foreach ( (array) $tempheaders as $header ) {
						if ( strpos( $header, ':' ) === false ) {
							if ( false !== stripos( $header, 'boundary=' ) ) {
								$parts = preg_split( '/boundary=/i', trim( $header ) );
								$boundary = trim( str_replace( array( "'", '"' ), '', $parts[1] ) );
							}
							continue;
						}
						// Explode them out.
						list($name, $content) = explode( ':', trim( $header ), 2 );

						// Cleanup crew.
						$name = trim( $name );
						$content = trim( $content );

						switch ( strtolower( $name ) ) {
                            case 'x-mailin-tag':
                            case 'content-type':
								$headers[ trim( $name ) ] = trim( $content );
								break;
                            case 'from':
								if ( strpos( $content, '<' ) !== false ) {
									// So... making my life hard again?
									$from_name = substr( $content, 0, strpos( $content, '<' ) - 1 );
									$from_name = str_replace( '"', '', $from_name );
									$from_name = trim( $from_name );

									$from_email = substr( $content, strpos( $content, '<' ) + 1 );
									$from_email = str_replace( '>', '', $from_email );
									$from_email = trim( $from_email );
								} else {
									$from_name = '';
									$from_email = trim( $content );
								}
								break;

							case 'bcc':
								if ( strpos( $content, '<') !== false )
								{
									$content = str_replace('<', '', $content);
									$content = str_replace('>', '', $content);
								}
								$data['bcc'] = array();
								$bcc_content = explode(',', $content);
								foreach ($bcc_content as $key => $value) {
									if ( ! empty( $value )) {
										$data['bcc'][] = array('email' => $value);
									}
								}
								break;
							case 'cc':
								if ( strpos( $content, '<') !== false )
								{
									$content = str_replace('<', '', $content);
									$content = str_replace('>', '', $content);
								}
								$data['cc'] = array();
								$cc_content = explode(',', $content);
								foreach ($cc_content as $key => $value) {
									if ( ! empty( $value )) {
										$data['cc'][] = array('email' => $value);
									}
								}
								break;
							case 'reply-to':
								if ( strpos( $content, '<') !== false )
								{
									$reply_content = substr( $content, strpos( $content, '<' ) + 1 );
									$reply_content = str_replace( '>', '', $reply_content );
									$reply = trim( $reply_content );
								} else {
									$reply = trim( $content );
								}

								if (!empty($reply)) {
									$data['replyTo'] = ['email' =>  trim( $reply )];
								}
								break;
							default:
								break;
						}
					}
				}
			}

			// Set destination addresses.
			if ( ! is_array( $to ) ) {
				$to = explode( ',', preg_replace( '/\s+/', '', $to ) ); // strip all whitespace.
			}

			$processed_to = array();
			foreach ( $to as $email ) {
				if ( is_array( $email ) ) {
					$processed_to[] = $email;
				} else {
					$processed_to[] = ['email' => $email];
				}
			}
			$data['to'] = $processed_to;


			// Attachments.
			$attachment_content = array();
			if ( ! empty( $attachments ) ) {
				foreach ( $attachments as $attachment ) {
					if ( !empty( $attachment ) ) {
						$content = self::getAttachmentStruct( $attachment );
						if ( ! is_wp_error( $content ) ) {
							$attachment_content = array_merge( $attachment_content, $content );
						}
					}
				}
				if ( !empty( $attachment_content ) ) {
					$data["attachment"] = array($attachment_content);
				}
			}

			// Common transformations for the HTML part.
			// If it is text/plain, New line break found.
			if ( strpos( $message, '</table>' ) === false && strpos( $message, '</div>' ) === false ) {
				if ( strpos( $message, "\n" ) !== false ) {
					if ( is_array( $message ) ) {
						foreach ( $message as &$value ) {
							$value['content'] = preg_replace( '#<(https?://[^*]+)>#', '$1', $value['content'] );
							$value['content'] = nl2br( $value['content'] );
						}
					} else {
						$message = preg_replace( '#<(https?://[^*]+)>#', '$1', $message );
						$message = nl2br( $message );
					}
				}
			}
			// Sending...
			$data['sender'] = ['email' => $from_email, 'name' => $from_name ];
			$data['subject'] = $subject;
			$data['htmlContent'] = $message;

			try {
				$sent = Pakat_API_Manager::send_email( $data );
				return $sent;
			} catch ( Exception $e ) {
				return new WP_Error( $e->getMessage() );
			}
		}

		/**
		 * @param string $path - attachment file path
		 * @return array|WP_Error
		 */
		static function getAttachmentStruct( $path ) {

			$struct = array();

			try {

				if ( ! @is_file( $path ) ) {
					throw new Exception( $path . ' is not a valid file.' );
				}

				$filename = basename( $path );

				if ( ! function_exists( 'get_magic_quotes' ) ) {
					/**
					 * @return bool
					 */
					function get_magic_quotes() {
						return false;
					}
				}
				if ( ! function_exists( 'set_magic_quotes' ) ) {
					/**
					 * @param $value
					 * @return bool
					 */
					function set_magic_quotes( $value ) {
						return true;
					}
				}

				$isMagicQuotesSupported = version_compare( PHP_VERSION, '5.3.0', '<' )
					&& function_exists( 'get_magic_quotes_runtime' )
					&& function_exists( 'set_magic_quotes_runtime' );

				if ( $isMagicQuotesSupported ) {
					// Escape linters check.
					$getMagicQuotesRuntimeFunc = 'get_magic_quotes_runtime';
					$setMagicQuotesRuntimeFunc = 'set_magic_quotes_runtime';

					// Save magic quotes value.
					$magicQuotes = $getMagicQuotesRuntimeFunc();
					$setMagicQuotesRuntimeFunc (0);
				}

				$file_buffer = file_get_contents( $path );
				$file_buffer = chunk_split( base64_encode( $file_buffer ), 76, "\n" );

				if ( $isMagicQuotesSupported ) {
					// Restore magic quotes value.
					$setMagicQuotesRuntimeFunc($magicQuotes);
				}

				$struct["name"]     = $filename;
				$struct["content"]     = $file_buffer;

			} catch ( Exception $e ) {
				return new WP_Error( 'Error creating the attachment structure: ' . $e->getMessage() );
			}

			return $struct;
		}

		/**
		 * Create custom page for form preview
		 *
		 * @param array $query_vars - query.
		 * @return array
		 */
		function pakat_query_vars( $query_vars ) {
			$query_vars[] = 'pakat_form';
			return $query_vars;
		}

		/**
		 * Parse request
		 *
		 * @param mixed $wp - object.
		 */
		function pakat_parse_request( &$wp ) {
			if ( array_key_exists( 'pakat_form', $wp->query_vars ) ) {
				include 'inc/pakat-form-preview.php';
				exit();
			}
		}

		/**
		 * Load Text domain.
		 */
		static function LoadTextDomain() {
			// Load lang file.
			$i18n_file_name = 'pakat_lang';
			$locale = apply_filters( 'plugin_locale', get_locale(), $i18n_file_name );
			// $locale = 'fr_FR';
			$filename = plugin_dir_path( __FILE__ ) . '/lang/' . $i18n_file_name . '-' . $locale . '.mo';
			load_textdomain( 'pakat_lang', $filename );
		}

		/**
		 * Notice the language is difference than site's language
		 */
		static function language_admin_notice() {
			if ( ! get_option( Pakat_Manager::LANGUAGE_OPTION_NAME ) ) {
				$lang_prefix = substr( get_bloginfo( 'language' ), 0, 2 );
				$lang = self::getLanguageName( $lang_prefix );
				$class = 'error';
				$message = sprintf( 'Please note that your Pakat account is in %s, but Pakat WordPress plugin is only available in English / French for now. Sorry for inconvenience.', $lang );
				if ( 'en' !== $lang_prefix && 'fr' !== $lang_prefix ) {
					echo ( "<div class=\"$class\" style='margin-left: 2px;margin-bottom: 4px;'> <p>$message<a class='' href='?dismiss_admin_lang_notice=1'> No problem...</a></p></div>" );
				}
			}
		}

		/**
		 * Notice wp_mail is not pospakatle
		 */
		static function wpMailNotices() {
			if ( self::$wp_mail_conflict ) {
				echo ( '<div class="error"><p>' . __( 'You cannot use Pakat SMTP now because wp_mail has been declared by another process or plugin. ', 'pakat_lang' ) . '</p></div>' );
			}
		}

		/**
		 * Names of languages.
		 *
		 * @param string $prefix - language.
		 * @return mixed
		 */
		public static function getLanguageName( $prefix = 'en' ) {
			$lang = array();
			$lang['de'] = 'Deutsch';
			$lang['en'] = 'English';
			$lang['zh'] = '中文';
			$lang['ru'] = 'Русский';
			$lang['fi'] = 'suomi';
			$lang['fr'] = 'Français';
			$lang['nl'] = 'Nederlands';
			$lang['sv'] = 'Svenska';
			$lang['it'] = 'Italiano';
			$lang['ro'] = 'Română';
			$lang['hu'] = 'Magyar';
			$lang['ja'] = '日本語';
			$lang['es'] = 'Español';
			$lang['vi'] = 'Tiếng Việt';
			$lang['ar'] = 'العربية';
			$lang['pt'] = 'Português';
			$lang['pb'] = 'Português do Brasil';
			$lang['pl'] = 'Polski';
			$lang['gl'] = 'galego';
			$lang['tr'] = 'Turkish';
			$lang['et'] = 'Eesti';
			$lang['hr'] = 'Hrvatski';
			$lang['eu'] = 'Euskera';
			$lang['el'] = 'Ελληνικά';
			$lang['ua'] = 'Українська';
			$lang['ko'] = '한국어';

			return $lang[ $prefix ];
		}

		/**
		 * Create language sidebar for wpml plugin.
		 */
		public function pakat_create_language_sidebar() {
			$languages = apply_filters( 'wpml_active_languages', array() );
			$page = isset( $_GET['page'] ) ? sanitize_text_field( $_GET['page'] ) : '';
			$action = isset( $_GET['action'] ) ? sanitize_text_field( $_GET['action'] ) : '';
			$frmID = isset( $_GET['id'] ) ? sanitize_text_field( $_GET['id'] ) : '';
			$pID = isset( $_GET['pid'] ) ? sanitize_text_field( $_GET['pid'] ) : '';
			$parent = true;
			if ( '' !== $frmID && '' !== $pID ) {
				$lang = Pakat_Forms_Lang::get_lang( $frmID, $pID );
				$parent = false;
			} else {
				$lang = ICL_LANGUAGE_CODE;
				if ( '' !== $frmID && '' === $pID ) {
					$pID = $frmID;

				}
			}

			if ( 'pakat_page_form' === $page && 'edit' === $action ) {
				?>
				<div class="panel panel-default text-left box-border-box  pakat-small-content">
					<div class="panel-heading"><strong><?php esc_attr_e( 'About Pakat', 'pakat_lang' ); ?></strong></div>
					<div class="panel-body">
						<p>
							<label for='pakat_form_language'><?php esc_attr_e( 'Language of this form:', 'pakat_lang' ); ?> </label>
							<select id="pakat_form_lang" name="pakat_form_lang" data-selected="">
								<?php
								foreach ( $languages as $language ) {
									$selected = ($language['code'] == $lang) ? 'selected' : '';
									if ( $language['code'] == $lang && true === $parent ) {
										$option_text = '<option value="" ' . $selected . '>' . $language['native_name'] . '</option>';
									} else {
										$exist = Pakat_Forms_Lang::get_form_ID( $pID, $language['language_code'] );

										if ( null === $exist ) {
											continue;
										} else {
											$option_text = ( 'selected' === $selected ) ? sprintf( '<option value="" selected>%s</option>', $language['native_name'] ) : sprintf( '<option value="?page=%s&action=%s&pid=%s&lang=%s" %s >%s</option>', esc_attr( $_REQUEST['page'] ), 'edit', absint( $pID ), $language['language_code'], $selected, $language['native_name'] );
										}
									}
									echo $option_text ;
								}
								?>
							</select>
						</p>
						<div class="pakat_form_translate">
							<p>
								<label><?php esc_attr_e( 'Translate this form', 'pakat_lang' ); ?></label>
							</p>
							<table width="100%" class="pakat_form_trans_table" style="border: 1px solid #8cceea;">
								<tr>
									<?php
									foreach ( $languages as $language ) {
										if ( $language['code'] == $lang ) {
											continue;
										}
										?>
										<th style="text-align: center;"><img
													src="<?php echo esc_url( $language['country_flag_url'] ); ?>"></th>
										<?php
									}
									?>
								</tr>
								<tr style="background-color: #EFF8FC;">
									<?php
									foreach ( $languages as $language ) {
										if ( $language['code'] == $lang ) {
											continue;
										}
										if ( '' === $pID ) {
											$img_src = plugins_url( 'img/add_translation_disabled.png', __FILE__ );
											$td = '<img src="' . $img_src . '" style="margin:2px;">';
										} else {
											$exist = Pakat_Forms_Lang::get_form_ID( $pID, $language['language_code'] );

											if ( null === $exist ) {
												$img_src = plugins_url( 'img/add_translation.png', __FILE__ );

												$href = sprintf( '<a class="pakat-form-redirect" href="?page=%s&action=%s&pid=%s&lang=%s" style="width: 20px; text-align: center;padding: 2px 1px;">', esc_attr( $_REQUEST['page'] ), 'edit', absint( $pID ), $language['language_code'] );
												$td = $href . '<img src="' . $img_src . '" style="margin:2px;"></a>';
											} else {
												$img_src = plugins_url( 'img/edit_translation.png', __FILE__ );
												$href = sprintf( '<a class="pakat-form-redirect" href="?page=%s&action=%s&id=%s&pid=%s&lang=%s" style="width: 20px; text-align: center;padding: 2px 1px;">', esc_attr( $_REQUEST['page'] ), 'edit', absint( $exist ), absint( $pID ), $language['language_code'] );
												$td = $href . '<img src="' . $img_src . '" style="margin:2px;"></a>';
											}
										}
										?>
										<td style="text-align: center;"><?php echo $td; ?></td>
										<?php
									}
									?>
								</tr>
							</table>
						</div>
						<?php if ( isset( $_GET['pid'] ) ) { ?>
							<div class="pakat-form-duplicate">
								<button class="btn btn-default pakat-duplicate-btn"><?php esc_attr_e( 'Copy content from origin form', 'pakat_lang' ); ?></button>
								<span class="pakat-spin"><i
											class="fa fa-circle-o-notch fa-spin fa-lg"></i>&nbsp;&nbsp;</span>
								<i title="<?php echo esc_attr_e( 'Copy content from origin form', 'pakat_lang' ); ?>"
								   data-container="body" data-toggle="popover" data-placement="left"
								   data-content="<?php echo esc_attr_e( 'You can copy contents from origin form. You need to translate the contents by this language.', 'pakat_lang' ); ?>"
								   data-html="true" class="fa fa-question-circle popover-help-form"></i>
							</div>
						<?php } ?>
					</div>
				</div>
				<?php
			}
		}

		public function ajax_get_country_prefix() {
            check_ajax_referer( 'pakat_front_ajax_nonce', 'security' );
            $sms_manager = new Pakat_SMS_Code();
            $country_list = $sms_manager->get_sms_code_list();
            $country_list_html = '';
            foreach ( $country_list as $item => $value ) {
                $flg_url = plugins_url( 'img/flags/', __FILE__ ).strtolower($item).'.png';
                $item_html = '<li class="pakat-country-prefix" data-country-code="'.$item.'" data-dial-code="'.$value["code"].'"><div class="pakat-flag-box"><div class="pakat-flag '.$item.'" style="background-image: url('.$flg_url.')"></div><span>'.$value['name'].'</span><span class="pakat-dial-code">+'.$value['code'].'</span></div></li>';
                $country_list_html .= $item_html;
            }
            wp_send_json($country_list_html);
        }

        /**
         * @param string $postAttribute
         * @param array $pakatAttributes
         * @return null|string the corresponding pakat attribute or null if not found
         */
        private function getCorrespondingPakatAttribute($postAttribute, $pakatAttributes)
        {
            $normalizedPostAttribute = strtoupper(sanitize_text_field($postAttribute));
            foreach ($pakatAttributes as $pakatAttribute) {
                if ($normalizedPostAttribute == strtoupper($pakatAttribute)) {
                    return $pakatAttribute;
                }
            }

            return null;
        }
    }

	add_action( 'pakat_init', 'pakat_init' );
	add_filter( 'widget_text', 'do_shortcode' );

	/**
	 * Plugin entry point Process.
	 */
	function pakat_init() {
		Pakat_Manager::LoadTextDomain();
		new Pakat_Manager();
	}

	do_action( 'pakat_init' );
}
