#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Newsletter, SMTP, Email marketing and Subscribe forms by "
"Pakat\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-05-29 11:29+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.2; wp-5.7.2\n"
"X-Domain: pakat"

#. URI of the plugin
#. Author URI of the plugin
msgid "https://www.pakat.net/?r=wporg"
msgstr ""

#. Description of the plugin
msgid ""
"Manage your contact lists, subscription forms and all email and marketing-"
"related topics from your wp panel, within one single plugin"
msgstr ""

#. Name of the plugin
msgid "Newsletter, SMTP, Email marketing and Subscribe forms by Pakat"
msgstr ""

#. Author of the plugin
msgid "Pakat"
msgstr ""
